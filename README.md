# iParapheur Android

[![pipeline](https://gitlab.libriciel.fr/i-parapheur/iParapheur-Android/badges/develop/pipeline.svg)](https://gitlab.libriciel.fr/i-parapheur/iParapheur-Android/commits/develop) [![coverage](https://gitlab.libriciel.fr/i-parapheur/iParapheur-Android/badges/develop/coverage.svg)](https://gitlab.libriciel.fr/i-parapheur/iParapheur-Android/commits/develop) [![lines_of_code](https://sonarqube.libriciel.fr/api/project_badges/measure?project=iParapheur-Android&metric=ncloc)](https://sonarqube.libriciel.fr/dashboard?id=iParapheur-Android)  
[![quality_gate](https://sonarqube.libriciel.fr/api/project_badges/measure?project=iParapheur-Android&metric=alert_status)](https://sonarqube.libriciel.fr/dashboard?id=iParapheur-Android) [![maintenability](https://sonarqube.libriciel.fr/api/project_badges/measure?project=iParapheur-Android&metric=sqale_rating)](https://sonarqube.libriciel.fr/dashboard?id=iParapheur-Android) [![reliability](https://sonarqube.libriciel.fr/api/project_badges/measure?project=iParapheur-Android&metric=reliability_rating)](https://sonarqube.libriciel.fr/dashboard?id=iParapheur-Android) [![security](https://sonarqube.libriciel.fr/api/project_badges/measure?project=iParapheur-Android&metric=security_rating)](https://sonarqube.libriciel.fr/dashboard?id=iParapheur-Android)

iParapheur native client for Android devices


## Usage

Packaged `.apk` builds are available to download in the CI artifacts, and the Tags section.



## Compatibilities charts

|   Certificates    |     Support      |
| ----------------- |:----------------:|
| Class 0 (.bks)    |        ✓         |
| RGS* (.bks)       |        ✓         |
| RGS** (hardware)  |  (coming ~2020)  |


|     Devices           |  Support  |
| --------------------- |:---------:|
| Chrome OS             |     ✘     |
| Android < 4.3         |     ✘     |
| Android >= 4.4        |     ✓     |
| Tablets (screen > 7") |     ✓     |
| Phones (screen < 7")  | (pending) |
