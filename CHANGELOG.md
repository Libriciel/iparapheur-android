
# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),  
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [1.6.3] - 2021-04-15
[1.6.3]: https://gitlab.libriciel.fr/i-parapheur/iParapheur-Android/tags/1.6.3

#### Fixed
- Application Id


## [1.6.2] - 2021-04-12
[1.6.2]: https://gitlab.libriciel.fr/i-parapheur/iParapheur-Android/tags/1.6.2

#### Added
- Direct certificate import with "Open with..."
- 30min session on certificate password
- Hints on icon's longpress
- SHA256 compatible signature
- IP4.6 compatible signature
- IP4.7 compatible signature

#### Removed
- Certificate import from Download folder


## [1.6.1] - 2019-04-29
[1.6.1]: https://gitlab.libriciel.fr/i-parapheur/iParapheur-Android/tags/1.6.1

#### Added
- Gitlab-CI
- SonarQube support
- Auto-packaging in a signed APK

#### Changed
- Switch from `m.` to `m-`
- MupdfAndroid as a simple library, instead of a (painful) submodule

#### Removed
- (Temporary) Check of Adullact G3 certificate


## [1.6.0] - 2017-07-20
[1.6.0]: https://gitlab.libriciel.fr/i-parapheur/iParapheur-Android/tags/1.6.0

#### Added
- Cachet action


## [1.5.8] - 2017-01-18
[1.5.8]: https://gitlab.libriciel.fr/i-parapheur/iParapheur-Android/tags/1.5.8

#### Fixed
- Crash fix


## [1.5.7] - 2016-12-21
[1.5.7]: https://gitlab.libriciel.fr/i-parapheur/iParapheur-Android/tags/1.5.7

#### Added
- Certificate import documentation, displayed in the settings
- Download icon in the desk view
- Offline consultation for downloaded files

#### Changed
- Minor UI changes
- MuPdf-Android library update


## [1.5.6] - 2016-09-19
[1.5.6]: https://gitlab.libriciel.fr/i-parapheur/iParapheur-Android/tags/1.5.6

#### Fixed
- Proper build for Google Play


## [1.5.5] - 2016-09-19
[1.5.5]: https://gitlab.libriciel.fr/i-parapheur/iParapheur-Android/tags/1.5.5

#### Added
- Floating Action button

#### Changed
- MuPdf-Android library update, commonly with iDelibre
