/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import fr.libriciel.iparapheur.model.Action;
import fr.libriciel.iparapheur.model.Circuit;
import fr.libriciel.iparapheur.model.Document;
import fr.libriciel.iparapheur.model.Dossier;
import fr.libriciel.iparapheur.model.EtapeCircuit;

import static fr.libriciel.iparapheur.model.Action.EMAIL;
import static fr.libriciel.iparapheur.model.Action.ENREGISTRER;
import static fr.libriciel.iparapheur.model.Action.JOURNAL;
import static fr.libriciel.iparapheur.model.Action.REJET;
import static fr.libriciel.iparapheur.model.Action.VISA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


@RunWith(RobolectricTestRunner.class)
public class DossierUtilsTest {


    @Test public void areDetailsAvailable() {

        List<EtapeCircuit> etapes = new ArrayList<>();
        etapes.add(new EtapeCircuit(null, true, false, "Bureau 01", "Signataire 01", Action.SIGNATURE.toString(), null));
        etapes.add(new EtapeCircuit(null, false, true, null, null, Action.VISA.toString(), null));
        Circuit circuit = new Circuit(etapes, "PKCS#7\\/single", true);

        ArrayList<Document> documentList = new ArrayList<>();
        documentList.add(new Document("id_01", null, 0, false, false));
        documentList.add(new Document("id_02", null, 0, true, false));

        Dossier dossier01 = new Dossier(null, null, null, null, null, null, null, null, false);
        dossier01.setCircuit(circuit);
        dossier01.setDocumentList(documentList);

        Dossier dossier02 = new Dossier(null, null, null, null, null, null, null, null, false);
        dossier02.setCircuit(circuit);

        Dossier dossier03 = new Dossier(null, null, null, null, null, null, null, null, false);
        dossier03.setDocumentList(documentList);

        // Checks

        assertTrue(DossierUtils.areDetailsAvailable(dossier01));
        assertFalse(DossierUtils.areDetailsAvailable(dossier02));
        assertFalse(DossierUtils.areDetailsAvailable(dossier03));
    }


    @SuppressWarnings("ConstantConditions") @Test public void findCurrentDocument() {

        ArrayList<Document> documentList = new ArrayList<>();
        documentList.add(new Document("id_01", null, 0, false, false));
        documentList.add(new Document("id_02", null, 0, true, false));

        Dossier dossier = new Dossier(null, null, null, null, null, null, null, null, false);
        dossier.setDocumentList(documentList);

        // Checks

        assertNull(DossierUtils.findCurrentDocument(null, "id_01"));
        assertEquals(DossierUtils.findCurrentDocument(dossier, null).getId(), "id_01");
        assertEquals(DossierUtils.findCurrentDocument(dossier, "id_01").getId(), "id_01");
        assertEquals(DossierUtils.findCurrentDocument(dossier, "id_02").getId(), "id_02");
    }


    @Test public void haveActions() {

        Dossier dossier01 = new Dossier(null, null, null, new HashSet<>(Arrays.asList(EMAIL, JOURNAL, ENREGISTRER)), null, null, null, null, false);
        Dossier dossier02 = new Dossier(null, null, null, new HashSet<>(Arrays.asList(VISA, EMAIL, JOURNAL, ENREGISTRER)), null, null, null, null, false);
        Dossier dossier03 = new Dossier(null, null, null, new HashSet<>(Arrays.asList(REJET, EMAIL, ENREGISTRER)), null, null, null, null, false);
        Dossier dossier04 = new Dossier(null, null, null, null, null, null, null, null, false);

        // Checks

        assertFalse(DossierUtils.haveActions(dossier01));
        assertTrue(DossierUtils.haveActions(dossier02));
        assertTrue(DossierUtils.haveActions(dossier03));
        assertFalse(DossierUtils.haveActions(dossier04));
    }


    @Test public void getMainDocumentsAndAnnexes() {

        Dossier emptyDossier = new Dossier(null, null, null, null, null, null, null, null, false);
        emptyDossier.setDocumentList(new ArrayList<Document>());

        ArrayList<Document> documentList = new ArrayList<>();
        documentList.add(new Document("id_01", null, 0, true, false));
        documentList.add(new Document("id_02", null, 0, false, false));
        documentList.add(new Document("id_03", null, 0, false, false));
        documentList.add(new Document("id_04", null, 0, true, false));

        Dossier dossier = new Dossier(null, null, null, null, null, null, null, null, false);
        dossier.setDocumentList(documentList);

        List<Document> mainDocumentList = DossierUtils.getMainDocuments(dossier);
        List<Document> annexesList = DossierUtils.getAnnexes(dossier);

        // Checks

        assertEquals(DossierUtils.getMainDocuments(emptyDossier), new ArrayList<Document>());
        assertEquals(DossierUtils.getAnnexes(emptyDossier), new ArrayList<Document>());

        assertEquals(mainDocumentList.size(), 2);
        assertEquals(mainDocumentList.get(0).getId(), "id_01");
        assertEquals(mainDocumentList.get(1).getId(), "id_04");

        assertEquals(annexesList.size(), 2);
        assertEquals(annexesList.get(0).getId(), "id_02");
        assertEquals(annexesList.get(1).getId(), "id_03");
    }


    @Test public void buildCreationDateComparator() {

        Dossier dossier01 = new Dossier("dossier01", null, null, null, null, null, new Date(50000L), null, false);
        Dossier dossier02 = new Dossier("dossier02", null, null, null, null, null, new Date(60000L), null, false);
        Dossier dossier03 = new Dossier("dossier03", null, null, null, null, null, new Date(70000L), null, false);
        Dossier dossier04 = new Dossier("dossier04", null, null, null, null, null, new Date(80000L), null, false);

        List<Dossier> dossierList = new ArrayList<>();
        dossierList.add(dossier03);
        dossierList.add(null);
        dossierList.add(dossier04);
        dossierList.add(dossier02);
        dossierList.add(dossier01);

        Collections.sort(dossierList, DossierUtils.buildCreationDateComparator());

        assertNull(dossierList.get(0));
        assertEquals(dossierList.get(1), dossier01);
        assertEquals(dossierList.get(2), dossier02);
        assertEquals(dossierList.get(3), dossier03);
        assertEquals(dossierList.get(4), dossier04);
    }

}