/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


@RunWith(RobolectricTestRunner.class)
public class StringUtilsTest {


    @Test public void areNotEmpty() {

        assertTrue(StringsUtils.areNotEmpty("Test"));

        assertFalse(StringsUtils.areNotEmpty(null, "Test"));
        assertFalse(StringsUtils.areNotEmpty("", "Test"));
        assertFalse(StringsUtils.areNotEmpty(""));
        assertFalse(StringsUtils.areNotEmpty(null, ""));
        assertFalse(StringsUtils.areNotEmpty());
    }


    @Test public void endsWithIgnoreCase() {

        assertTrue(StringsUtils.endsWithIgnoreCase("test 123", "test 123"));
        assertTrue(StringsUtils.endsWithIgnoreCase("test 123", "T 123"));
        assertTrue(StringsUtils.endsWithIgnoreCase("test 123", "123"));
        assertTrue(StringsUtils.endsWithIgnoreCase("test 123", ""));

        assertFalse(StringsUtils.endsWithIgnoreCase("test 123", "test 1234"));
        assertFalse(StringsUtils.endsWithIgnoreCase("test 123", "2"));
        assertFalse(StringsUtils.endsWithIgnoreCase("test 123", null));
        assertFalse(StringsUtils.endsWithIgnoreCase(null, null));
    }


    @Test public void parseIso8601Date() {

        Date parsedDate = StringsUtils.parseIso8601Date("2016-12-25T23:45:00");
        assertNotNull(parsedDate);

        assertNull(StringsUtils.parseIso8601Date("999999"));
        assertNull(StringsUtils.parseIso8601Date(""));
        assertNull(StringsUtils.parseIso8601Date(null));
    }


    @Test public void serializeToIso8601Date() {
        assertNotNull(StringsUtils.serializeToIso8601Date(new Date(1482705900000L)));
    }


    @Test public void fixIssuerDnX500NameStringOrder() {

        String input = "OU=ADULLACT-Projet,E=systeme@adullact.org,CN=AC ADULLACT Projet\\, g2,O=ADULLACT-Projet,ST=Herault,C=FR";
        String value = StringsUtils.fixIssuerDnX500NameStringOrder(input);
        String expected = "EMAIL=systeme@adullact.org,CN=AC ADULLACT Projet\\, g2,OU=ADULLACT-Projet,O=ADULLACT-Projet,ST=Herault,C=FR";

        assertEquals(expected, value);
    }


    @Test public void fixUrl() {
        assertEquals("parapheur", StringsUtils.fixUrl("https://m.parapheur/iparapheur.plop//"));
        assertEquals("parapheur.test.adullact.org", StringsUtils.fixUrl("http://parapheur.test.adullact.org/parapheur/test"));
        assertEquals("parapheur.test.adullact.org", StringsUtils.fixUrl("m.parapheur.test.adullact.org/"));
        assertEquals("parapheur.test.adullact.org", StringsUtils.fixUrl("parapheur.test.adullact.org"));
        assertEquals("parapheur", StringsUtils.fixUrl("https://parapheur"));
        assertEquals("parapheur", StringsUtils.fixUrl("https://m.parapheur"));
        assertEquals("parapheur", StringsUtils.fixUrl("https://m-parapheur"));
        assertEquals("parapheur", StringsUtils.fixUrl("m.parapheur"));
        assertEquals("parapheur", StringsUtils.fixUrl("m-parapheur"));
        assertEquals("parapheur", StringsUtils.fixUrl("parapheur"));
    }


    @Test public void getLocalizedSmallDate() {
        assertNotNull(StringsUtils.getLocalizedSmallDate(new Date(1482705900000L)));
        assertEquals("???", StringsUtils.getLocalizedSmallDate(null));
    }


    @Test public void getVerySmallDate() {
        assertEquals("25/12", StringsUtils.getVerySmallDate(new Date(1482705900000L)));
        assertEquals("???", StringsUtils.getVerySmallDate(null));
    }


    @Test public void getSmallTime() {
        assertNotNull(StringsUtils.getSmallTime(new Date(1482705900000L)));
        assertEquals("???", StringsUtils.getSmallTime(null));
    }


    @Test public void isUrlValid() {
        assertTrue(StringsUtils.isUrlValid("parapheur"));
        assertTrue(StringsUtils.isUrlValid("parapheur.test.adullact.org"));
        assertFalse(StringsUtils.isUrlValid(":::::"));
        assertFalse(StringsUtils.isUrlValid(""));
        assertFalse(StringsUtils.isUrlValid(null));
    }


    @Test public void nullableBooleanValueOf() {

    }

}