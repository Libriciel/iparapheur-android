/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.model;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import fr.libriciel.iparapheur.utils.CollectionUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;


@RunWith(RobolectricTestRunner.class)
public class DocumentTest {

    private static Gson sGson = CollectionUtils.buildGsonWithDateParser();


    @Test public void gsonParse() {

        String incorrectJsonString = "[[{]   \"id\": \"Value 01\" , \"collectivite\": [\"Value 01-01\"  ]]";
        String correctJsonString = "{" + "    \"size\": 50000," + "    \"visuelPdf\": true," + "    \"isMainDocument\": true," + "    \"pageCount\": 5," + "    \"attestState\": 0," + "    \"id\": \"id_01\"," + "    \"name\": \"name 01.pdf\"," + "    \"canDelete\": true," + "    \"isLocked\": true" + "}";

        Document incorrectObjectParsed;
        try { incorrectObjectParsed = sGson.fromJson(incorrectJsonString, Document.class); } catch (JsonSyntaxException ex) { incorrectObjectParsed = null; }

        Document correctObjectParsed = sGson.fromJson(correctJsonString, Document.class);
        Document document = new Document("id_01", "name 01.pdf", 50000, true, true);
        document.setPagesAnnotations(null);
        document.setSyncDate(null);
        document.setParent(null);

        // Checks

        assertNull(incorrectObjectParsed);
        assertNotNull(correctObjectParsed);

        assertEquals(document, correctObjectParsed);
        assertEquals(correctObjectParsed.toString(), document.toString());
        assertEquals(correctObjectParsed.getId(), document.getId());
        assertEquals(correctObjectParsed.getName(), document.getName());
        assertEquals(correctObjectParsed.getSize(), document.getSize());
        assertEquals(correctObjectParsed.isMainDocument(), document.isMainDocument());
        assertEquals(correctObjectParsed.isPdfVisual(), document.isPdfVisual());
        assertEquals(correctObjectParsed.getPagesAnnotations(), document.getPagesAnnotations());
        assertEquals(correctObjectParsed.getSyncDate(), document.getSyncDate());
        assertEquals(correctObjectParsed.getParent(), document.getParent());
    }

}