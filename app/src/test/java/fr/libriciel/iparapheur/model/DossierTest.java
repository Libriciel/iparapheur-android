/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.model;

import com.google.gson.Gson;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import fr.libriciel.iparapheur.utils.CollectionUtils;

import static fr.libriciel.iparapheur.model.Action.AVIS_COMPLEMENTAIRE;
import static fr.libriciel.iparapheur.model.Action.EMAIL;
import static fr.libriciel.iparapheur.model.Action.ENREGISTRER;
import static fr.libriciel.iparapheur.model.Action.JOURNAL;
import static fr.libriciel.iparapheur.model.Action.REJET;
import static fr.libriciel.iparapheur.model.Action.SECRETARIAT;
import static fr.libriciel.iparapheur.model.Action.SIGNATURE;
import static fr.libriciel.iparapheur.model.Action.TRANSFERT_SIGNATURE;
import static fr.libriciel.iparapheur.model.Action.VISA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


@RunWith(RobolectricTestRunner.class)
public class DossierTest {

    private static Gson sGson = CollectionUtils.buildGsonWithDateParser();


    @Test public void fromJsonArray() {

        // Parsed data

        String incorrectArrayJsonString = "[[{]   \"id\": \"Value 01\" , \"collectivite\": [\"Value 01-01\"  ]]";
        String correctArrayJsonString = "[{" + "    \"total\": 2," + "    \"protocol\": \"ACTES\"," + "    \"actionDemandee\": \"SIGNATURE\"," + "    \"isSent\": true," + "    \"type\": \"Type 01\"," + "    \"bureauName\": \"Bureau 01\"," + "    \"creator\": \"Creator 01\"," + "    \"id\": \"id_01\"," + "    \"title\": \"Title 01\"," + "    \"pendingFile\": 0," + "    \"banetteName\": \"Dossiers à traiter\"," + "    \"skipped\": 1," + "    \"sousType\": \"Subtype 01\"," + "    \"isSignPapier\": true," + "    \"isXemEnabled\": true," + "    \"hasRead\": true," + "    \"readingMandatory\": true," + "    \"documentPrincipal\": {" + "        \"id\": \"id_doc_01\"," + "        \"name\": \"Document 01\"" + "    }," + "    \"locked\": true," + "    \"actions\": [\"ENREGISTRER\", \"EMAIL\", \"JOURNAL\", \"SECRETARIAT\", \"REJET\", \"VISA\", \"SIGNATURE\", \"TRANSFERT_SIGNATURE\", \"AVIS_COMPLEMENTAIRE\"]," + "    \"isRead\": true," + "    \"dateEmission\": 1392829477205," + "    \"includeAnnexes\": true" + "}, {" + "    \"total\": 2," + "    \"type\": \"Type 02\"," + "    \"id\": \"id_02\"," + "    \"title\": \"Title 02\"," + "    \"sousType\": \"Subtype 02\"" + "}]";

        List<Dossier> incorrectArrayParsed = Dossier.fromJsonArray(incorrectArrayJsonString, sGson);
        List<Dossier> correctArrayParsed = Dossier.fromJsonArray(correctArrayJsonString, sGson);

        // Valid types

        HashSet<Action> dossier01ActionsSet = new HashSet<>(Arrays.asList(ENREGISTRER, EMAIL, JOURNAL, SECRETARIAT, REJET, SIGNATURE, TRANSFERT_SIGNATURE, AVIS_COMPLEMENTAIRE));
        HashSet<Action> dossier02ActionsSet = new HashSet<>(Arrays.asList(VISA, SIGNATURE));

        Dossier dossier01 = new Dossier("id_01", "Title 01", SIGNATURE, dossier01ActionsSet, "Type 01", "Subtype 01", new Date(1392829477205L), null, true);
        Dossier dossier02 = new Dossier("id_02", "Title 02", VISA, dossier02ActionsSet, "Type 02", "Subtype 02", null, null, false);

        dossier01.setSyncDate(null);
        dossier02.setSyncDate(null);
        dossier01.setParent(null);
        dossier02.setParent(null);
        dossier01.setChildrenDocuments(null);
        dossier02.setChildrenDocuments(null);

        // Checks

        assertNull(incorrectArrayParsed);
        assertNotNull(correctArrayParsed);

        assertEquals(correctArrayParsed.get(0).toString(), dossier01.toString());
        assertEquals(correctArrayParsed.get(0).getId(), dossier01.getId());
        assertEquals(correctArrayParsed.get(0).getName(), dossier01.getName());
        assertEquals(correctArrayParsed.get(0).getActionDemandee(), dossier01.getActionDemandee());
        assertEquals(correctArrayParsed.get(0).getType(), dossier01.getType());
        assertEquals(correctArrayParsed.get(0).getSousType(), dossier01.getSousType());
        assertEquals(correctArrayParsed.get(0).getActions(), dossier01.getActions());
        assertEquals(correctArrayParsed.get(0).isSignPapier(), dossier01.isSignPapier());
        assertEquals(correctArrayParsed.get(0).getDocumentList(), dossier01.getDocumentList());
        assertEquals(correctArrayParsed.get(0).getDateLimite(), dossier01.getDateLimite());
        assertEquals(correctArrayParsed.get(0).getDateCreation(), dossier01.getDateCreation());
        assertEquals(correctArrayParsed.get(0).getSyncDate(), dossier01.getSyncDate());
        assertEquals(correctArrayParsed.get(0).getParent(), dossier01.getParent());
        assertEquals(correctArrayParsed.get(0).getChildrenDocuments(), dossier01.getChildrenDocuments());

        assertEquals(correctArrayParsed.get(1).toString(), dossier02.toString());
        assertEquals(correctArrayParsed.get(1).getId(), dossier02.getId());
        assertEquals(correctArrayParsed.get(1).getName(), dossier02.getName());
        assertEquals(correctArrayParsed.get(1).getActionDemandee(), dossier02.getActionDemandee());
        assertEquals(correctArrayParsed.get(1).getType(), dossier02.getType());
        assertEquals(correctArrayParsed.get(1).getSousType(), dossier02.getSousType());
        assertEquals(correctArrayParsed.get(1).getActions(), dossier02.getActions());
        assertEquals(correctArrayParsed.get(1).isSignPapier(), dossier02.isSignPapier());
        assertEquals(correctArrayParsed.get(1).getDocumentList(), dossier02.getDocumentList());
        assertEquals(correctArrayParsed.get(1).getDateLimite(), dossier02.getDateLimite());
        assertEquals(correctArrayParsed.get(1).getDateCreation(), dossier02.getDateCreation());
        assertEquals(correctArrayParsed.get(1).getSyncDate(), dossier02.getSyncDate());
        assertEquals(correctArrayParsed.get(1).getParent(), dossier02.getParent());
        assertEquals(correctArrayParsed.get(1).getChildrenDocuments(), dossier02.getChildrenDocuments());
    }


    @Test public void fromJsonObject() {

        // Parsed data

        String incorrectJsonString = "[[{]   \"id\": \"Value 01\" , \"collectivite\": [\"Value 01-01\"  ]]";
        String correctJsonString = "{" + "    \"title\": \"Dossier 01\"," + "    \"nomTdT\": \"pas de TdT\"," + "    \"includeAnnexes\": true," + "    \"locked\": null," + "    \"readingMandatory\": true," + "    \"acteursVariables\": []," + "    \"dateEmission\": 1455553363700," + "    \"visibility\": \"public\"," + "    \"isRead\": true," + "    \"actionDemandee\": \"VISA\"," + "    \"status\": null," + "    \"documents\": [{" + "        \"size\": 224260," + "        \"visuelPdf\": false," + "        \"isMainDocument\": true," + "        \"pageCount\": 1," + "        \"attestState\": 0," + "        \"id\": \"0ad04448-4424-416a-8e10-36a160b0cb9d\"," + "        \"name\": \"aaef91b4-d135-40ce-80dd-cb4ef1e0ffbc.pdf\"," + "        \"canDelete\": false," + "        \"isLocked\": false" + "    }, {" + "        \"id\": \"5321b0e1-e055-4cff-a69c-5cd358da12e1\"," + "        \"isLocked\": false," + "        \"attestState\": 0," + "        \"visuelPdf\": false," + "        \"size\": 16153," + "        \"canDelete\": false," + "        \"name\": \"20160205_1540_texte_reponse.odt\"," + "        \"isMainDocument\": false" + "    }]," + "    \"id\": \"id_01\"," + "    \"isSignPapier\": true," + "    \"dateLimite\": 1455553363700," + "    \"hasRead\": true," + "    \"isXemEnabled\": true," + "    \"actions\": [\"ENREGISTRER\", \"EMAIL\", \"JOURNAL\", \"REJET\", \"VISA\", \"TRANSFERT_ACTION\", \"AVIS_COMPLEMENTAIRE\", \"GET_ATTEST\"]," + "    \"banetteName\": \"Dossiers à traiter\"," + "    \"type\": \"Type 01\"," + "    \"canAdd\": true," + "    \"protocole\": \"aucun\"," + "    \"metadatas\": {" + "        \"cu:Canton\": {" + "            \"values\": [\"Castries\", \"Grabels\", \"Jacou\", \"Juvignac\", \"Mauguio\"]," + "            \"default\": \"Castries\"," + "            \"mandatory\": \"false\"," + "            \"value\": \"\"," + "            \"realName\": \"Canton concerné\"," + "            \"type\": \"STRING\"," + "            \"editable\": \"false\"" + "        }" + "    }," + "    \"xPathSignature\": null," + "    \"sousType\": \"SubType 01\"," + "    \"bureauName\": \"Bureau 01\"," + "    \"isSent\": true" + "}";

        Dossier incorrectObjectParsed = Dossier.fromJsonObject(incorrectJsonString, sGson);
        Dossier correctObjectParsed = Dossier.fromJsonObject(correctJsonString, sGson);

        // Checks

        assertNull(incorrectObjectParsed);
        assertNotNull(correctObjectParsed);

        assertEquals(correctObjectParsed.getId(), "id_01");
        assertEquals(correctObjectParsed.getName(), "Dossier 01");
        assertEquals(correctObjectParsed.getActionDemandee(), Action.VISA);
        assertEquals(correctObjectParsed.getType(), "Type 01");
        assertEquals(correctObjectParsed.getSousType(), "SubType 01");
        assertEquals(correctObjectParsed.getDateCreation(), new Date(1455553363700L));
        assertEquals(correctObjectParsed.getDateLimite(), new Date(1455553363700L));
        assertEquals(correctObjectParsed.getActions().size(), 9);
        assertTrue(correctObjectParsed.isSignPapier());
        assertEquals(correctObjectParsed.getDocumentList().size(), 2);
    }


    @SuppressWarnings({"ObjectEqualsNull"}) @Test public void dossierEquals() {

        Dossier dossier01 = new Dossier("id_01", null, null, null, null, null, null, null, false);
        Dossier dossier01bis = new Dossier("id_01", null, null, null, null, null, null, null, false);
        Dossier dossier02 = new Dossier(null, "id_02", null, null, null, null, null, null, false);

        // Checks

        assertEquals(dossier01, dossier01bis);
        assertEquals("id_01", dossier01.getId());

        assertNotEquals(dossier01, dossier02);
        assertNotEquals(null, dossier01);
        assertNotEquals("id_02", dossier01);
        assertNotEquals(1, dossier01);
    }


    @Test public void dossierHashCode() {

        Dossier dossier01 = new Dossier("id_01", null, null, null, null, null, null, null, false);
        Dossier dossier02 = new Dossier(null, null, null, null, null, null, null, null, false);

        // Checks

        assertEquals(dossier01.hashCode(), "id_01".hashCode());
        assertEquals(dossier02.hashCode(), -1);
    }

}