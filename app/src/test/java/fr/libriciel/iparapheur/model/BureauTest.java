/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.model;

import com.google.gson.Gson;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.List;

import fr.libriciel.iparapheur.utils.CollectionUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;


@RunWith(RobolectricTestRunner.class)
public class BureauTest {

    private static Gson sGson = CollectionUtils.buildGsonWithDateParser();


    @Test public void fromJsonArray() {

        // Parsed data

        String incorrectArrayJsonString = "[[{]   \"id\": \"Value 01\" , \"collectivite\": [\"Value 01-01\"  ]]";
        String correctArrayJsonString = "[" + "{" + "    \"hasSecretaire\": false," + "    \"collectivite\": \"Collectivité 01 \\\"\\\\/%@&éè\"," + "    \"description\": null," + "    \"en-preparation\": 0," + "    \"nodeRef\": \"workspace:\\/\\/SpacesStore\\/44abe93c-16d7-4e00-b561-f6d1b8b6c1d3\"," + "    \"shortName\": \"C1\"," + "    \"en-retard\": 5," + "    \"image\": \"\"," + "    \"show_a_venir\": null," + "    \"habilitation\": {" + "        \"traiter\": null," + "        \"secretariat\": null," + "        \"archivage\": null," + "        \"transmettre\": null" + "    }," + "    \"a-archiver\": 27," + "    \"a-traiter\": 10," + "    \"id\": \"id_01\"," + "    \"isSecretaire\": false," + "    \"name\": \"Name 01 \\\"\\/%@&éè\"," + "    \"retournes\": 13," + "    \"dossiers-delegues\": 59" + "}, {" + "    \"hasSecretaire\": true," + "    \"collectivite\": \"Collectivité 02 \\\"\\\\/%@&éè\"," + "    \"description\": \"Description 02 \\\"\\\\/%@&éè\"," + "    \"en-preparation\": 1," + "    \"nodeRef\": \"id_02\"," + "    \"shortName\": \"C2\"," + "    \"en-retard\": 5," + "    \"image\": null," + "    \"show_a_venir\": null," + "    \"habilitation\": {" + "        \"traiter\": null," + "        \"secretariat\": null," + "        \"archivage\": null," + "        \"transmettre\": null" + "    }," + "    \"a-archiver\": 33," + "    \"isSecretaire\": false," + "    \"name\": \"Name 02 \\\"\\/%@&éè\"," + "    \"retournes\": 10," + "    \"dossiers-delegues\": 0" + "}]";

        List<Bureau> incorrectArrayParsed = Bureau.fromJsonArray(incorrectArrayJsonString, sGson);
        List<Bureau> correctArrayParsed = Bureau.fromJsonArray(correctArrayJsonString, sGson);

        // Valid types

        Bureau bureau01 = new Bureau("id_01", "Name 01 \"/%@&éè", 10, 5);
        Bureau bureau02 = new Bureau("workspace://SpacesStore/id_02", "Name 02 \"/%@&éè", 0, 0);

        bureau01.setSyncDate(null); bureau02.setSyncDate(null); bureau01.setParent(null); bureau02.setParent(null); bureau01.setChildrenDossiers(null);
        bureau02.setChildrenDossiers(null);

        // Checks

        assertNull(incorrectArrayParsed); assertNotNull(correctArrayParsed);

        assertEquals(correctArrayParsed.get(0).toString(), bureau01.toString()); assertEquals(correctArrayParsed.get(0).getId(), bureau01.getId());
        assertEquals(correctArrayParsed.get(0).getTitle(), bureau01.getTitle());
        assertEquals(correctArrayParsed.get(0).getLateCount(), bureau01.getLateCount());
        assertEquals(correctArrayParsed.get(0).getTodoCount(), bureau01.getTodoCount());
        assertEquals(correctArrayParsed.get(0).getSyncDate(), bureau01.getSyncDate());
        assertEquals(correctArrayParsed.get(0).getParent(), bureau01.getParent());
        assertEquals(correctArrayParsed.get(0).getChildrenDossiers(), bureau01.getChildrenDossiers());

        assertEquals(correctArrayParsed.get(1).toString(), bureau02.toString()); assertEquals(correctArrayParsed.get(1).getId(), bureau02.getId());
        assertEquals(correctArrayParsed.get(1).getTitle(), bureau02.getTitle());
        assertEquals(correctArrayParsed.get(1).getLateCount(), bureau02.getLateCount());
        assertEquals(correctArrayParsed.get(1).getTodoCount(), bureau02.getTodoCount());
        assertEquals(correctArrayParsed.get(1).getSyncDate(), bureau02.getSyncDate());
        assertEquals(correctArrayParsed.get(1).getParent(), bureau02.getParent());
        assertEquals(correctArrayParsed.get(1).getChildrenDossiers(), bureau02.getChildrenDossiers());

        assertEquals(bureau01, correctArrayParsed.get(0)); assertEquals(bureau02, correctArrayParsed.get(1));
    }

}