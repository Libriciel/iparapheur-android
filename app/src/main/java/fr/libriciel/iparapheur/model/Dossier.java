/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.model;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import fr.libriciel.iparapheur.utils.ActionUtils;
import lombok.Data;


@Data
@DatabaseTable(tableName = "Folder")
public class Dossier {

    public static final String DB_FIELD_ID = "Id";
    private static final String DB_FIELD_NAME = "Name";
    private static final String DB_FIELD_ACTION_ASKED = "ActionAsked";
    private static final String DB_FIELD_TYPE = "Type";
    private static final String DB_FIELD_SUBTYPE = "SubType";
    private static final String DB_FIELD_CREATION_DATE = "CreationDate";
    private static final String DB_FIELD_LATE_DATE = "LateDate";
    private static final String DB_FIELD_ACTIONS = "Actions";
    private static final String DB_FIELD_IS_PAPER_SIGN = "IsPaperSign";
    private static final String DB_FIELD_DOCUMENTS = "Documents";
    private static final String DB_FIELD_SYNC = "Sync";
    private static final String DB_FIELD_DESK = "Desk";


    @DatabaseField(columnName = DB_FIELD_ID, id = true, index = true)  //
    private String id;

    @DatabaseField(columnName = DB_FIELD_NAME, canBeNull = false, defaultValue = "")  //
    @SerializedName("title")  //
    private String name;

    @DatabaseField(columnName = DB_FIELD_ACTION_ASKED, dataType = DataType.ENUM_STRING, canBeNull = false, defaultValue = "VISA")  //
    private Action actionDemandee;

    @DatabaseField(columnName = DB_FIELD_TYPE)  //
    private String type;

    @DatabaseField(columnName = DB_FIELD_SUBTYPE)  //
    private String sousType;

    @DatabaseField(columnName = DB_FIELD_CREATION_DATE)  //
    @SerializedName("dateEmission")  //
    private Date dateCreation;

    @DatabaseField(columnName = DB_FIELD_LATE_DATE)  //
    private Date dateLimite;

    @DatabaseField(columnName = DB_FIELD_ACTIONS, dataType = DataType.SERIALIZABLE)  //
    private HashSet<Action> actions;

    @DatabaseField(columnName = DB_FIELD_IS_PAPER_SIGN)  //
    @SerializedName("isSignPapier")  //
    private boolean isSignPapier;

    @SerializedName("documents")  //
    private List<Document> documentList = new ArrayList<>();

    @ForeignCollectionField(columnName = DB_FIELD_DOCUMENTS)  //
    private transient ForeignCollection<Document> childrenDocuments;

    @DatabaseField(columnName = DB_FIELD_SYNC)  //
    private Date syncDate;

    @DatabaseField(columnName = DB_FIELD_DESK, foreign = true, foreignAutoRefresh = true)  //
    private transient Bureau parent;

    private Circuit circuit;


    public Dossier() {}


    public Dossier(String id, String name, Action actionDemandee, HashSet<Action> actions, String type, String sousType, Date dateCreation,
                   Date dateLimite, boolean isSignPapier) {
        this.id = id;
        this.name = name;
        this.actionDemandee = actionDemandee;
        this.actions = actions;
        this.type = type;
        this.sousType = sousType;
        this.dateCreation = dateCreation;
        this.dateLimite = dateLimite;
        this.isSignPapier = isSignPapier;
    }


    /**
     * Static parser, useful for Unit tests
     *
     * @param jsonArrayString data as a Json array, serialized with some {@link org.json.JSONArray#toString}.
     * @param gson            passed statically to prevent re-creating it.
     */
    public static @Nullable List<Dossier> fromJsonArray(@NonNull String jsonArrayString, @NonNull Gson gson) {

        Type listDossierType = new TypeToken<ArrayList<Dossier>>() {}.getType();

        try {
            ArrayList<Dossier> dossiersParsed = gson.fromJson(jsonArrayString, listDossierType);

            // Fix default value on parse.
            // There is no easy way (@annotation) to do it with Gson,
            // So we're doing it here instead of overriding everything.
            for (Dossier dossier : dossiersParsed)
                ActionUtils.fixActions(dossier);

            return dossiersParsed;
        } catch (JsonSyntaxException e) {
            return null;
        }
    }


    /**
     * Static parser, useful for Unit tests
     *
     * @param jsonObjectString data as a Json array, serialized with some {@link org.json.JSONArray#toString}.
     * @param gson             passed statically to prevent re-creating it.
     */
    public static @Nullable Dossier fromJsonObject(@NonNull String jsonObjectString, @NonNull Gson gson) {

        try {
            Dossier dossierParsed = gson.fromJson(jsonObjectString, Dossier.class);

            // Fix default value on parse.
            // There is no easy way (@annotation) to do it with Gson,
            // So we're doing it here instead of overriding everything.
            if (dossierParsed != null) ActionUtils.fixActions(dossierParsed);

            return dossierParsed;
        } catch (JsonSyntaxException e) {
            return null;
        }
    }


    @Override public boolean equals(Object o) {

        if (o == null)
            return false;
        if (o instanceof Dossier)
            return TextUtils.equals(id, ((Dossier) o).getId());
        else if (o instanceof String) return TextUtils.equals(id, (String) o);

        return false;
    }


    @Override public int hashCode() {
        return (id != null) ? id.hashCode() : -1;
    }

}
