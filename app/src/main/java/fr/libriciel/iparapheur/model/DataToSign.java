/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.libriciel.iparapheur.service.rest.request.RemoteDocument;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class DataToSign {

    protected List<String> dataToSignBase64List = new ArrayList<>();
    private Long signatureDateTime;
    private Map<String, String> payload = new HashMap<>();

    private @JsonIgnore boolean legacy = false;
    private @JsonIgnore String format;
    private @JsonIgnore String digestAlgorithm;
    private @JsonIgnore List<RemoteDocument> remoteDocuments = new ArrayList<>();


    @JsonProperty("dataToSignBase64List")
    private void parseDataToSignBase64List(String hashes) {
        dataToSignBase64List.addAll(Arrays.asList(hashes.split(",")));
    }

}

