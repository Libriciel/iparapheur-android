/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;


@Data
public class ParapheurType {

    private final @SerializedName("id") String name;
    private final @SerializedName("sousTypes") List<String> subTypes;


    /**
     * Static parser, useful for Unit tests
     *
     * @param jsonArrayString data as a Json array, serialized with some {@link org.json.JSONArray#toString}.
     * @param gson            passed statically to prevent re-creating it.
     */
    public static @Nullable List<ParapheurType> fromJsonArray(@NonNull String jsonArrayString, @NonNull Gson gson) {

        Type typologyType = new TypeToken<ArrayList<ParapheurType>>() {}.getType();

        try {
            return gson.fromJson(jsonArrayString, typologyType);
        } catch (JsonSyntaxException e) {
            return null;
        }
    }


    public ParapheurType(@NonNull String name, @NonNull List<String> subTypes) {
        this.name = name;
        this.subTypes = subTypes;
    }

}
