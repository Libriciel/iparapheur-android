/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.model;

import android.util.Base64;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.libriciel.iparapheur.utils.StringsUtils;
import lombok.Data;
import lombok.NoArgsConstructor;

import static android.util.Base64.NO_WRAP;


@Data
@NoArgsConstructor
@JsonRootName("signatureInformations")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SignInfo {


    private String format;
    private List<String> hashesToSignBase64 = new ArrayList<>();
    private List<String> pesIds = new ArrayList<>();
    private @JsonIgnore boolean legacy = false;


    @JsonProperty("hash")
    private void parseHashes(String hashes) {
        for (String hash : hashes.split(",")) {
            byte[] hashBytes = StringsUtils.hexDecode(hash);
            String hashBase64 = Base64.encodeToString(hashBytes, NO_WRAP);
            hashesToSignBase64.add(hashBase64);
        }
    }


    @JsonProperty("pesid")
    private void parsePesIds(String pesIdsString) {
        pesIds.addAll(Arrays.asList(pesIdsString.split(",")));
    }


}
