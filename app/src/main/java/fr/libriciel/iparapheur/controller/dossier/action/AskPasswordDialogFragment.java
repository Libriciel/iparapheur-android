/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.controller.dossier.action;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import fr.libriciel.iparapheur.R;

import static android.app.Activity.RESULT_OK;


public class AskPasswordDialogFragment extends DialogFragment {

    public static final String FRAGMENT_TAG = "ask_password_dialog_fragment";
    public static final int REQUEST_CODE_ASK_PASSWORD = 11911;   // Because A-S-K = 01-19-11
    public static final @SuppressWarnings("squid:S2068") String RESULT_BUNDLE_EXTRA_PASSWORD = "password";

    private static final String ARGUMENTS_SELECTED_ALIAS = "selected_alias";

    private EditText mPasswordEditText;
    private TextView mPasswordLabel;
    private String mSelectedAlias;


    public static AskPasswordDialogFragment newInstance(String alias) {

        AskPasswordDialogFragment fragment = new AskPasswordDialogFragment();

        Bundle args = new Bundle();
        args.putString(ARGUMENTS_SELECTED_ALIAS, alias);

        fragment.setArguments(args);

        return fragment;
    }


    // <editor-fold desc="LifeCycle">


    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mSelectedAlias = getArguments().getString(ARGUMENTS_SELECTED_ALIAS);
        }
    }


    @Override public @NonNull Dialog onCreateDialog(Bundle savedInstanceState) {

        // Create view

        View view = View.inflate(getActivity(), R.layout.action_dialog_ask_password, null);

        mPasswordEditText = view.findViewById(R.id.action_ask_password_edittext);
        mPasswordLabel = view.findViewById(R.id.action_ask_password_label);

        // Set listeners

        mPasswordEditText.setOnFocusChangeListener((v, hasFocus) -> mPasswordLabel.setActivated(hasFocus));

        mPasswordEditText.setOnEditorActionListener((v, actionId, event) -> {
            onValidButtonClicked();
            dismissAllowingStateLoss();
            return true;
        });

        // Build Dialog

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppTheme_Main_Dialog);
        builder.setView(view);
        builder.setTitle(mSelectedAlias);
        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> onValidButtonClicked());
        builder.setNegativeButton(android.R.string.cancel, (dialog, id) -> onCancelButtonClicked());

        return builder.create();
    }


    // </editor-fold desc="LifeCycle">


    private void onValidButtonClicked() {

        Intent intent = new Intent();
        intent.putExtra(RESULT_BUNDLE_EXTRA_PASSWORD, mPasswordEditText.getText().toString());

        getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, intent);
    }


    private void onCancelButtonClicked() {
        dismissAllowingStateLoss();
    }

}
