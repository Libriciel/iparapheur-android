/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.controller.dossier.action;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import fr.libriciel.iparapheur.R;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;


public class SignaturePapierConfirmDialogFragment extends DialogFragment {

    public static final String FRAGMENT_TAG = "sign_papier_dialog_fragment";
    public static final int REQUEST_CODE_SIGN_PAPIER = 1601160518;    // Because P-A-P-E-R = 16-01-16-05-18


    public static SignaturePapierConfirmDialogFragment newInstance() {
        return new SignaturePapierConfirmDialogFragment();
    }


    @Override public @NonNull Dialog onCreateDialog(Bundle savedInstanceState) {

        // Create view

        View view = View.inflate(getActivity(), R.layout.action_dialog_signature_papier_confirmation, null);

        // Build Dialog

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppTheme_Main_Dialog);
        builder.setView(view);
        builder.setPositiveButton(R.string.signature_papier_transform, (dialog, which) -> getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, null));
        builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_CANCELED, null));

        return builder.create();
    }

}
