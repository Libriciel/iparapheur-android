/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.controller.dossier.filter;

import android.content.Context;
import android.widget.ArrayAdapter;

import fr.libriciel.iparapheur.R;
import fr.libriciel.iparapheur.model.Filter;


public class FilterAdapter extends ArrayAdapter<Filter> {


    public FilterAdapter(Context context) {
        super(context, R.layout.activity_dossiers_toolbar_spinner_cell_main);
        setDropDownViewResource(R.layout.activity_dossiers_toolbar_spinner_cell_dropdown);
    }


    @Override public int getCount() {
        return MyFilters.INSTANCE.getFilters(getContext()).size() + 1;
    }


    @Override public Filter getItem(int position) {
        Filter filter;
        if (position < MyFilters.INSTANCE.getFilters(getContext()).size()) {
            filter = MyFilters.INSTANCE.getFilters(getContext()).get(position);
        }
        else {
            filter = new Filter(Filter.EDIT_FILTER_ID);
            filter.setName(getContext().getResources().getString(R.string.action_filtrer));
        }
        return filter;
    }


    @Override public int getPosition(Filter item) {
        int position = MyFilters.INSTANCE.getFilters(getContext()).indexOf(item);
        return (position == -1) ? MyFilters.INSTANCE.getFilters(getContext()).size() + 1 : position;
    }

}
