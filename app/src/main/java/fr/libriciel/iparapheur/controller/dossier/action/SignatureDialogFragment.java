/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.controller.dossier.action;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import fr.libriciel.iparapheur.R;
import fr.libriciel.iparapheur.databinding.ActionDialogSignatureBinding;
import fr.libriciel.iparapheur.model.Circuit;
import fr.libriciel.iparapheur.model.DataToSign;
import fr.libriciel.iparapheur.model.Dossier;
import fr.libriciel.iparapheur.service.rest.api.RestClientFactory;
import fr.libriciel.iparapheur.service.rest.request.RemoteDocument;
import fr.libriciel.iparapheur.utils.AccountUtils;
import fr.libriciel.iparapheur.utils.CollectionUtils;
import fr.libriciel.iparapheur.utils.FilesUtils;
import fr.libriciel.iparapheur.utils.IParapheurException;
import fr.libriciel.iparapheur.utils.RsaSigner;
import io.sentry.Sentry;

import static android.app.Activity.RESULT_OK;
import static android.util.Base64.DEFAULT;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static android.widget.Toast.LENGTH_SHORT;


public class SignatureDialogFragment extends DialogFragment {

    public static final String FRAGMENT_TAG = "signature_dialog_fragment";
    public static final int REQUEST_CODE_SIGNATURE = 19090714;    // Because S-I-G-N = 19-09-07-14
    public static final int RESULT_CODE_SIGN_PAPIER = 1601160518;    // Because P-A-P-E-R = 16-01-16-05-18

    private static final String LOG_TAG = "SignatureDialogFragment";
    private static final String ARGUMENTS_DOSSIERS = "dossiers";
    private static final String ARGUMENTS_BUREAU_ID = "bureau_id";

    private static final long PASSWORD_DELAY_MS = 30 * 60 * 1000;
    private static final Map<Pair<String, String>, Pair<Long, String>> PASSWORD_CACHE = new HashMap<>();

    protected EditText mPublicAnnotationEditText;
    protected EditText mPrivateAnnotationEditText;
    protected TextView mPublicAnnotationLabel;
    protected TextView mPrivateAnnotationLabel;
    protected Spinner mCertificateSpinner;
    protected Spinner mAliasesSpinner;
    protected TextView mCertificateLabel;
    protected TextView mAliasesLabel;

    private List<File> mCertificateFileList;
    private List<String> mCertificateNameList;
    private List<String> mAliasList;
    private String mBureauId;
    private ArrayList<Dossier> mDossierList;

    private File mSelectedCertificateFile;
    private String mSelectedCertificatePassword;


    public static @NonNull SignatureDialogFragment newInstance(@NonNull ArrayList<Dossier> dossiers, @NonNull String bureauId) {

        SignatureDialogFragment fragment = new SignatureDialogFragment();

        Bundle args = new Bundle();
        Gson gson = CollectionUtils.buildGsonWithDateParser();
        args.putString(ARGUMENTS_DOSSIERS, gson.toJson(dossiers));
        args.putString(ARGUMENTS_BUREAU_ID, bureauId);

        fragment.setArguments(args);

        return fragment;
    }


    // <editor-fold desc="LifeCycle">


    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            Gson gson = CollectionUtils.buildGsonWithDateParser();
            Type typologyType = new TypeToken<ArrayList<Dossier>>() {}.getType();

            try { mDossierList = gson.fromJson(getArguments().getString(ARGUMENTS_DOSSIERS), typologyType); } catch (JsonSyntaxException e) {
                mDossierList = new ArrayList<>();
            }

            mBureauId = getArguments().getString(ARGUMENTS_BUREAU_ID);
        }

        // Init values

        mCertificateNameList = new ArrayList<>();
        mAliasList = new ArrayList<>();
    }


    @Override public @NonNull Dialog onCreateDialog(Bundle savedInstanceState) {

        // Create view

        ActionDialogSignatureBinding binding = ActionDialogSignatureBinding.inflate(LayoutInflater.from(getActivity()));

        mPublicAnnotationEditText = binding.actionSignaturePublicAnnotation;
        mPrivateAnnotationEditText = binding.actionSignaturePrivateAnnotation;
        mPublicAnnotationLabel = binding.actionSignaturePublicAnnotationLabel;
        mPrivateAnnotationLabel = binding.actionSignaturePrivateAnnotationLabel;
        mCertificateSpinner = binding.actionSignatureChooseCertificateSpinner;
        mCertificateLabel = binding.actionSignatureChooseCertificateLabel;
        mAliasesSpinner = binding.actionSignatureChooseAliasSpinner;
        mAliasesLabel = binding.actionSignatureChooseAliasLabel;

        // Set Spinner adapters

        ArrayAdapter<String> certificatesArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, mCertificateNameList);
        certificatesArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCertificateSpinner.setAdapter(certificatesArrayAdapter);

        ArrayAdapter<String> aliasesArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, mAliasList);
        aliasesArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mAliasesSpinner.setAdapter(aliasesArrayAdapter);

        // Set listeners

        mPublicAnnotationEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v, boolean hasFocus) {
                mPublicAnnotationLabel.setActivated(hasFocus);
            }
        });

        mPrivateAnnotationEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override public void onFocusChange(View v, boolean hasFocus) {
                mPrivateAnnotationLabel.setActivated(hasFocus);
            }
        });

        mCertificateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                refreshAliasesSpinner();
            }


            @Override public void onNothingSelected(AdapterView<?> parent) {
                refreshAliasesSpinner();
            }
        });

        // Build Dialog

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppTheme_Main_Dialog);
        builder.setView(binding.getRoot());
        builder.setPositiveButton(R.string.action_signer, (dialog, which) -> {
            // Do nothing here because we override this button in the onStart() to change the close behaviour.
            // However, we still need this because on older versions of Android :
            // unless we pass a handler the button doesn't get instantiated
        });
        builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> onCancelButtonClicked());
        builder.setNeutralButton(R.string.action_sign_papier, (dialog, which) -> {
            // Do nothing here because we override this button in the onStart() to change the close behaviour.
            // However, we still need this because on older versions of Android :
            // unless we pass a handler the button doesn't get instantiated
        });

        return builder.create();
    }


    @Override public void onStart() {
        super.onStart();

        refreshCertificatesSpinner();
        refreshSignPapierButtonVisibility();
        retrieveMissingCircuitParameters();

        // Overriding the AlertDialog.Builder#setPositiveButton
        // To be able to manage a click without dismissing the popup.

        AlertDialog dialog = (AlertDialog) getDialog();
        if (dialog == null) return;

        Button signButton = dialog.getButton(Dialog.BUTTON_POSITIVE);
        signButton.setOnClickListener(v -> onSignButtonClicked());

        Button signPapierButton = dialog.getButton(Dialog.BUTTON_NEUTRAL);
        signPapierButton.setOnClickListener(v -> onSignPapierButtonClicked());
    }


    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if ((requestCode == AskPasswordDialogFragment.REQUEST_CODE_ASK_PASSWORD) && (resultCode == RESULT_OK)) {
            String password = data.getStringExtra(AskPasswordDialogFragment.RESULT_BUNDLE_EXTRA_PASSWORD);

            Pair<String, String> certPair = Pair.create(mCertificateSpinner.getSelectedItem().toString(), mAliasesSpinner.getSelectedItem().toString());
            Pair<Long, String> passwordTimed = Pair.create(SystemClock.uptimeMillis(), password);
            PASSWORD_CACHE.put(certPair, passwordTimed);

            new SignTask().execute(password);
            return;
        }
        else if ((requestCode == SignaturePapierConfirmDialogFragment.REQUEST_CODE_SIGN_PAPIER) && (resultCode == RESULT_OK)) {
            new SignPapierTask().execute();
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    // </editor-fold desc="LifeCycle">


    private void onSignButtonClicked() {

        // TODO : disable sign button, instead
        if (mAliasesSpinner.getSelectedItem() == null) {
            return;
        }

        Pair<String, String> certPair = Pair.create(mCertificateSpinner.getSelectedItem().toString(), mAliasesSpinner.getSelectedItem().toString());
        Pair<Long, String> cachedPassword = PASSWORD_CACHE.get(certPair);

        if ((cachedPassword != null) && ((SystemClock.uptimeMillis() - cachedPassword.first) < PASSWORD_DELAY_MS)) {
            new SignTask().execute(cachedPassword.second);
        }
        else {
            AskPasswordDialogFragment askFragment = AskPasswordDialogFragment.newInstance(mAliasesSpinner.getSelectedItem().toString());
            askFragment.setTargetFragment(this, AskPasswordDialogFragment.REQUEST_CODE_ASK_PASSWORD);
            askFragment.show(getActivity().getSupportFragmentManager(), AskPasswordDialogFragment.FRAGMENT_TAG);
        }

        // See #onActivityResult() for password retrieval
        // and SignTask#onPostExecute for popup dismiss.
    }


    private void onCancelButtonClicked() {
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_CANCELED, null);
        dismissAllowingStateLoss();
    }


    private void onSignPapierButtonClicked() {

        SignaturePapierConfirmDialogFragment confirmationDialog = SignaturePapierConfirmDialogFragment.newInstance();
        confirmationDialog.setTargetFragment(this, SignaturePapierConfirmDialogFragment.REQUEST_CODE_SIGN_PAPIER);
        confirmationDialog.show(getActivity().getSupportFragmentManager(), SignaturePapierConfirmDialogFragment.FRAGMENT_TAG);
    }


    private void refreshCertificatesSpinner() {

        Executor executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executor.execute(() -> {

            // Generate String List

            List<File> certificateList = FilesUtils.getBksFromCertificateFolder(getActivity());
            mCertificateNameList.clear();

            for (File certificateFile : certificateList) {
                mCertificateNameList.add(certificateFile.getName());
            }

            handler.post(() -> {
                ((ArrayAdapter<String>) mCertificateSpinner.getAdapter()).notifyDataSetChanged();
                refreshAliasesSpinner();
            });
        });
    }


    private void refreshAliasesSpinner() {

        // Default case

        if (mCertificateSpinner.getSelectedItem() == null) {
            return;
        }

        // Inflate aliases

        final String selectedCertificateName = mCertificateSpinner.getSelectedItem().toString();

        Executor executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executor.execute(() -> {

            // Default cases

            mCertificateFileList = FilesUtils.getBksFromCertificateFolder(getActivity());
            if (mCertificateFileList.isEmpty()) {
                return;
            }

            mSelectedCertificateFile = null;
            for (File certificateFile : mCertificateFileList)
                if (TextUtils.equals(certificateFile.getName(), selectedCertificateName)) mSelectedCertificateFile = certificateFile;

            if (mSelectedCertificateFile == null) {
                return;
            }

            // Retrieving Certificate password.

            SharedPreferences settings = getActivity().getSharedPreferences(FilesUtils.SHARED_PREFERENCES_CERTIFICATES_PASSWORDS, 0);
            mSelectedCertificatePassword = settings.getString(mSelectedCertificateFile.getName(), "");

            // Retrieving aliases from Certificate file.

            RsaSigner signer = new RsaSigner(mSelectedCertificateFile.getAbsolutePath(), mSelectedCertificatePassword, "", "");
            ArrayList<String> aliasList = new ArrayList<>();

            try {
                KeyStore keystore = signer.loadKeyStore();

                if (keystore != null) aliasList.addAll(Collections.list(keystore.aliases()));
            } catch (CertificateException | NoSuchAlgorithmException | KeyStoreException | IOException | UnrecoverableKeyException e) {
                Log.e(LOG_TAG, "Error getting Certificate", e);
            }

            // Inflate spinner

            mAliasList.clear();
            mAliasList.addAll(aliasList);

            handler.post(() -> {
                ((ArrayAdapter<String>) mAliasesSpinner.getAdapter()).notifyDataSetChanged();
            });
        });
    }


    private void retrieveMissingCircuitParameters() {

        Executor executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        executor.execute(() -> {

            for (Dossier dossier : mDossierList) {
                if (dossier.getCircuit() == null) {
                    try {
                        Circuit circuit = new RestClientFactory()
                                .createRestClient(AccountUtils.SELECTED_ACCOUNT, getActivity())
                                .getWorkflow(dossier.getId());

                        dossier.setCircuit(circuit);
                    } catch (IParapheurException e) {
                        Log.e(LOG_TAG, "Error getting workflow", e);
                        Sentry.capture(e);
                    }
                }
            }

            handler.post(this::refreshSignPapierButtonVisibility);
        });
    }


    private void refreshSignPapierButtonVisibility() {

        AlertDialog dialog = (AlertDialog) getDialog();
        if (dialog == null) return;

        boolean isDigitalSignatureMandatory = false;

        for (Dossier dossier : mDossierList) {
            if ((dossier.getCircuit() == null) || (dossier.getCircuit().isDigitalSignatureMandatory())) {
                isDigitalSignatureMandatory = true;
                break;
            }
        }

        dialog.getButton(Dialog.BUTTON_NEUTRAL).setVisibility(isDigitalSignatureMandatory ? GONE : VISIBLE);
    }


    private class SignTask extends AsyncTask<String, Void, Boolean> {

        private String mAnnotPub;
        private String mAnnotPriv;
        private String mSelectedAlias;
        private int mErrorMessage;


        @Override protected void onPreExecute() {
            super.onPreExecute();

            mAnnotPub = mPublicAnnotationEditText.getText().toString();
            mAnnotPriv = mPrivateAnnotationEditText.getText().toString();
            mSelectedAlias = mAliasesSpinner.getSelectedItem().toString();
            mErrorMessage = -1;
        }


        @SuppressLint("DefaultLocale") @Override protected Boolean doInBackground(String... passwordArg) {

            // Default cases

            if (isCancelled()) return false;

            if (mSelectedCertificateFile == null) {
                return false;
            }

            //

            String password = passwordArg[0];
            int total = mDossierList.size();
            for (int docIndex = 0; docIndex < total; docIndex++) {

                // Retrieve data to sign

                RsaSigner signer = new RsaSigner(mSelectedCertificateFile.getAbsolutePath(), mSelectedCertificatePassword, mSelectedAlias, password);
                DataToSign dataToSign = null;

                try {
                    signer.loadKeyStore();

                    dataToSign = new RestClientFactory()
                            .createRestClient(AccountUtils.SELECTED_ACCOUNT, getActivity())
                            .getDataToSign(mBureauId, mDossierList.get(docIndex).getId(), signer.getPublicKeyBase64());

                } catch (CertificateException | UnrecoverableKeyException | NoSuchAlgorithmException | KeyStoreException | IOException e) {
                    Log.e(LOG_TAG, "CertificateException | IOException", e);
                    Sentry.capture(e);
                    mErrorMessage = R.string.signature_error_message_error_opening_bks_file;
                } catch (IParapheurException e) {
                    Log.e(LOG_TAG, "Error performing task", e);
                    Sentry.capture(e);
                    mErrorMessage = R.string.signature_error_message_not_get_from_server;
                }

                if (dataToSign == null || dataToSign.getDataToSignBase64List().isEmpty()) {
                    return false;
                }

                // Sign data

                try {
                    signer.loadPrivateKey();

                    for (RemoteDocument remoteDocument : dataToSign.getRemoteDocuments()) {
                        String signValue = signer.sign(Base64.decode(remoteDocument.getDataToSignBase64(), DEFAULT), dataToSign.getDigestAlgorithm());
                        remoteDocument.setSignatureBase64(signValue);
                    }
                } catch (NoSuchAlgorithmException | KeyStoreException e) {
                    Log.e(LOG_TAG, "Cert exception", e);
                    Sentry.capture(e);
                    mErrorMessage = R.string.signature_error_message_incompatible_device;
                } catch (UnrecoverableKeyException e) {
                    Log.e(LOG_TAG, "UnrecoverableKeyException", e);
                    Sentry.capture(e);
                    mErrorMessage = R.string.signature_error_message_missing_alias_or_wrong_password;
                } catch (InvalidKeyException e) {
                    Log.e(LOG_TAG, "InvalidKeyException", e);
                    Sentry.capture(e);
                    mErrorMessage = R.string.signature_error_message_wrong_password;
                } catch (IllegalArgumentException e) {
                    Log.e(LOG_TAG, "IllegalArgumentException", e);
                    Sentry.capture(e);
                    mErrorMessage = R.string.signature_error_message_no_data_to_sign;
                } catch (SignatureException | IllegalStateException e) {
                    Log.e(LOG_TAG, "SignatureException | IllegalStateException", e);
                    Sentry.capture(e);
                    mErrorMessage = R.string.signature_error_message_unknown_error;
                    e.printStackTrace();
                }

                // Send result, if any

                Log.d(LOG_TAG, "Signature... ");

                if (isCancelled() || (mErrorMessage != -1)) {
                    return false;
                }

                try {
                    new RestClientFactory()
                            .createRestClient(AccountUtils.SELECTED_ACCOUNT, getActivity())
                            .sign(mBureauId, mDossierList.get(docIndex).getId(), dataToSign.getRemoteDocuments(), dataToSign.getSignatureDateTime(),
                                    signer.getPublicKeyBase64(), dataToSign.getFormat(), mAnnotPub, mAnnotPriv, dataToSign.isLegacy());
                } catch (IParapheurException e) {
                    Sentry.capture(e);
                    mErrorMessage = (e.getResId() > 0) ? e.getResId() : R.string.signature_error_message_not_sent_to_server;
                    Log.e(LOG_TAG, e.getLocalizedMessage());
                    e.printStackTrace();
                } catch (JsonProcessingException e) {
                    Sentry.capture(e);
                    mErrorMessage = R.string.signature_error_message_not_sent_to_server;
                    Log.e(LOG_TAG, e.getLocalizedMessage());
                    e.printStackTrace();
                }

                if (isCancelled()) {
                    return false;
                }
            }

            // If no error message, then the signature is successful.
            return (mErrorMessage == -1);
        }


        @Override protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);

            if (success) {
                getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, null);
                dismissAllowingStateLoss();
            }
            else if (getActivity() != null) {
                Toast.makeText(getActivity(), ((mErrorMessage != -1)
                                               ? mErrorMessage
                                               : R.string.signature_error_message_unknown_error), LENGTH_SHORT).show();
            }
        }

    }


    private class SignPapierTask extends AsyncTask<Void, Void, Boolean> {

        private int mErrorMessage;


        @Override protected void onPreExecute() {
            super.onPreExecute();
            mErrorMessage = -1;
        }


        @Override protected Boolean doInBackground(Void... voids) {

            // Default cases

            if (isCancelled()) return false;

            //

            int total = mDossierList.size();
            for (int docIndex = 0; docIndex < total; docIndex++) {

                if (isCancelled()) return false;

                // Send request

                try {
                    new RestClientFactory()
                            .createRestClient(AccountUtils.SELECTED_ACCOUNT, getActivity())
                            .signPapier(mDossierList.get(docIndex).getId(), mBureauId);
                } catch (IParapheurException e) {
                    Sentry.capture(e);
                    mErrorMessage = R.string.signature_error_message_not_sent_to_server;
                    Log.e(LOG_TAG, e.getLocalizedMessage());
                }
            }

            // If no error message, then the signature is successful.
            return (mErrorMessage == -1);
        }


        @Override protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);

            if (success) {
                getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_CODE_SIGN_PAPIER, null);
                dismissAllowingStateLoss();
            }
            else if (getActivity() != null) {
                Toast.makeText(
                        getActivity(),
                        (mErrorMessage != -1) ? mErrorMessage : R.string.signature_papier_unknown_error,
                        LENGTH_SHORT
                ).show();
            }
        }

    }

}
