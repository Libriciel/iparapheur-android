/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.controller.preferences;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import java.io.File;

import fr.libriciel.iparapheur.R;
import fr.libriciel.iparapheur.databinding.ActionDialogImportBinding;
import fr.libriciel.iparapheur.utils.FilesUtils;

import static android.content.DialogInterface.BUTTON_POSITIVE;


public class ImportCertificatesDialogFragment extends DialogFragment {

    public static final String FRAGMENT_TAG = "import_certificates_dialog_fragment";

    private static final String ARGUMENT_CERTIFICATE_PATH = "certificatePath";

    protected File mCertificateFile;

    protected EditText mPasswordEditText;
    protected TextView mPasswordLabel;


    public static ImportCertificatesDialogFragment newInstance(@NonNull File certificate) {

        ImportCertificatesDialogFragment fragment = new ImportCertificatesDialogFragment();

        Bundle args = new Bundle();
        args.putString(ARGUMENT_CERTIFICATE_PATH, certificate.getAbsolutePath());
        fragment.setArguments(args);

        return fragment;
    }


    // <editor-fold desc="LifeCycle">


    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

            String path = getArguments().getString(ARGUMENT_CERTIFICATE_PATH);
            if (path != null) {
                mCertificateFile = new File(path);
            }
        }
    }


    @Override public @NonNull Dialog onCreateDialog(Bundle savedInstanceState) {

        // Create view

        ActionDialogImportBinding binding = ActionDialogImportBinding.inflate(LayoutInflater.from(getActivity()));

        mPasswordEditText = binding.dialogImportPassword;
        mPasswordLabel = binding.dialogImportPasswordLabel;

        // Set listeners

        mPasswordEditText.setOnFocusChangeListener((v, hasFocus) -> mPasswordLabel.setActivated(hasFocus));

        mPasswordEditText.setOnEditorActionListener((v, actionId, event) -> {
            onImportButtonClicked();
            return true;
        });

        // Build Dialog

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppTheme_Main_Dialog);
        builder.setTitle(R.string.import_certificate);
        builder.setView(binding.getRoot());
        builder.setPositiveButton(R.string.import_certificate, (dialog, which) -> onImportButtonClicked());
        builder.setNegativeButton(android.R.string.cancel, (dialog, id) -> onCancelButtonClicked());

        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(dialog -> {
            Button positiveButton = alertDialog.getButton(BUTTON_POSITIVE);
            positiveButton.setOnClickListener(v -> onImportButtonClicked());
        });

        return alertDialog;
    }


    // </editor-fold desc="LifeCycle">


    private void onImportButtonClicked() {

        boolean success = FilesUtils.importCertificate(getActivity(), mCertificateFile, mPasswordEditText.getText().toString());

        if (success) {
            dismissAllowingStateLoss();
        }
    }


    private void onCancelButtonClicked() {
        dismissAllowingStateLoss();
    }

}
