/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.controller.preferences;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.gms.oss.licenses.OssLicensesMenuActivity;

import fr.libriciel.iparapheur.R;
import fr.libriciel.iparapheur.databinding.PreferencesMenuFragmentBinding;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PreferenceMenuFragmentListener} interface
 * to handle interaction events.
 * Use the {@link PreferencesMenuFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PreferencesMenuFragment extends Fragment implements View.OnClickListener {

    public static final String FRAGMENT_TAG = "preferences_menu_fragment";


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PreferencesMenuFragment.
     */
    public static PreferencesMenuFragment newInstance() {
        return new PreferencesMenuFragment();
    }


    public PreferencesMenuFragment() {
        // Required empty public constructor
    }


    // <editor-fold desc="LifeCycle">


    @Override public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        PreferencesMenuFragmentBinding binding = PreferencesMenuFragmentBinding.inflate(inflater, container, false);

        binding.preferencesAccount.setOnClickListener(this);
        binding.preferencesCertificates.setOnClickListener(this);
        binding.preferencesAbout.setOnClickListener(this);
        binding.preferencesLicenses.setOnClickListener(this);
        // binding.preferencesFilters.setOnClickListener(this);

        return binding.getRoot();
    }


    @Override public void onResume() {
        super.onResume();

        if (getActivity() instanceof AppCompatActivity) {
            AppCompatActivity parentActivity = (AppCompatActivity) getActivity();
            if (parentActivity.getSupportActionBar() != null) parentActivity.getSupportActionBar().setTitle(R.string.Settings);
        }
    }


    // </editor-fold desc="LifeCycle">


    // <editor-fold desc="OnClickListener">


    @Override public void onClick(View v) {

        // Determine which Fragment was clicked

        if (v.getId() == R.id.preferences_account) {
            Fragment clickedFragment = PreferencesAccountFragment.newInstance();
            ((PreferenceMenuFragmentListener) getActivity()).onMenuElementClicked(clickedFragment);
        }
        else if (v.getId() == R.id.preferences_certificates) {
            Fragment clickedFragment = PreferencesCertificatesFragment.newInstance();
            ((PreferenceMenuFragmentListener) getActivity()).onMenuElementClicked(clickedFragment);
        }
        else if (v.getId() == R.id.preferences_about) {
            Fragment clickedFragment = PreferencesAboutFragment.newInstance();
            ((PreferenceMenuFragmentListener) getActivity()).onMenuElementClicked(clickedFragment);
        }
        else if (v.getId() == R.id.preferences_licenses) {
            OssLicensesMenuActivity.setActivityTitle(getString(R.string.about_library_licenses_title));
            Intent intent = new Intent(getActivity(), OssLicensesMenuActivity.class);
            startActivity(intent);
        }
    }


    // </editor-fold desc="OnClickListener">


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface PreferenceMenuFragmentListener {

        void onMenuElementClicked(@NonNull Fragment fragment);

    }

}
