/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.controller.dossier.action;

import android.app.Activity;
import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import fr.libriciel.iparapheur.R;
import fr.libriciel.iparapheur.databinding.ActionDialogSealBinding;
import fr.libriciel.iparapheur.model.Action;
import fr.libriciel.iparapheur.model.Dossier;
import fr.libriciel.iparapheur.service.rest.api.RestClientFactory;
import fr.libriciel.iparapheur.utils.AccountUtils;
import fr.libriciel.iparapheur.utils.CollectionUtils;
import fr.libriciel.iparapheur.utils.IParapheurException;
import io.sentry.Sentry;

import static android.widget.Toast.LENGTH_SHORT;


public class SealDialogFragment extends DialogFragment {

    public static final String FRAGMENT_TAG = "seal_dialog_fragment";
    public static final int REQUEST_CODE_SEAL = 19050112;    // Because S-E-A-L = 19-05-01-12

    private static final String LOG_TAG = "SealDialogFragment";
    private static final String ARGUMENTS_DOSSIERS = "dossiers";
    private static final String ARGUMENTS_BUREAU_ID = "bureau_id";

    protected EditText mPublicAnnotationEditText;
    protected EditText mPrivateAnnotationEditText;
    protected TextView mPublicAnnotationLabel;
    protected TextView mPrivateAnnotationLabel;

    private String mBureauId;
    private ArrayList<Dossier> mDossierList;


    public static @NonNull SealDialogFragment newInstance(@NonNull ArrayList<Dossier> dossiers, @NonNull String bureauId) {

        SealDialogFragment fragment = new SealDialogFragment();

        Bundle args = new Bundle();
        Gson gson = CollectionUtils.buildGsonWithDateParser();
        args.putString(ARGUMENTS_DOSSIERS, gson.toJson(dossiers));
        args.putString(ARGUMENTS_BUREAU_ID, bureauId);

        fragment.setArguments(args);

        return fragment;
    }


    // <editor-fold desc="LifeCycle">


    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            Gson gson = CollectionUtils.buildGsonWithDateParser();
            Type typologyType = new TypeToken<ArrayList<Dossier>>() {}.getType();

            try { mDossierList = gson.fromJson(getArguments().getString(ARGUMENTS_DOSSIERS), typologyType); } catch (JsonSyntaxException e) {
                mDossierList = new ArrayList<>();
            }

            mBureauId = getArguments().getString(ARGUMENTS_BUREAU_ID);
        }
    }


    @Override public @NonNull Dialog onCreateDialog(Bundle savedInstanceState) {

        // Create view

        ActionDialogSealBinding binding = ActionDialogSealBinding.inflate(LayoutInflater.from(getActivity()));

        mPublicAnnotationEditText = binding.actionSealPublicAnnotation;
        mPrivateAnnotationEditText = binding.actionSealPrivateAnnotation;
        mPublicAnnotationLabel = binding.actionSealPublicAnnotationLabel;
        mPrivateAnnotationLabel = binding.actionSealPrivateAnnotationLabel;

        // Set listeners

        mPublicAnnotationEditText.setOnFocusChangeListener((v, hasFocus) -> mPublicAnnotationLabel.setActivated(hasFocus));

        mPrivateAnnotationEditText.setOnFocusChangeListener((v, hasFocus) -> mPrivateAnnotationLabel.setActivated(hasFocus));

        // Build Dialog

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppTheme_Main_Dialog);
        builder.setView(binding.getRoot());
        builder.setPositiveButton(R.string.action_seal, (dialog, which) -> {
            // Do nothing here because we override this button in the onStart() to change the close behaviour.
            // However, we still need this because on older versions of Android :
            // unless we pass a handler the button doesn't get instantiated
        });
        builder.setNegativeButton(android.R.string.cancel, (dialog, id) -> onCancelButtonClicked());

        return builder.create();
    }


    @Override public void onStart() {
        super.onStart();

        // Overriding the AlertDialog.Builder#setPositiveButton
        // To be able to manage a click without dismissing the popup.

        AlertDialog dialog = (AlertDialog) getDialog();
        if (dialog != null) {
            Button positiveButton = dialog.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(v -> onSealButtonClicked());
        }
    }

    // </editor-fold desc="LifeCycle">


    private void onSealButtonClicked() {

        new SealTask().execute();
        // See VisaTask#onPostExecute for popup dismiss.
    }


    private void onCancelButtonClicked() {
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_CANCELED, null);
    }


    private class SealTask extends AsyncTask<Void, Void, Boolean> {

        private String mAnnotPub;
        private String mAnnotPriv;
        private int mErrorMessage;


        @Override protected void onPreExecute() {
            super.onPreExecute();

            mAnnotPub = mPublicAnnotationEditText.getText().toString();
            mAnnotPriv = mPrivateAnnotationEditText.getText().toString();
            mErrorMessage = -1;
        }


        @Override protected Boolean doInBackground(Void... args) {

            if (isCancelled()) return false;

            for (Dossier dossier : mDossierList) {

                try {

                    if (dossier.getActions().contains(Action.CACHET)) {
                        new RestClientFactory()
                                .createRestClient(AccountUtils.SELECTED_ACCOUNT, getActivity())
                                .seal(dossier, mAnnotPub, mAnnotPriv, mBureauId);
                    }
                    else {
                        new RestClientFactory()
                                .createRestClient(AccountUtils.SELECTED_ACCOUNT, getActivity())
                                .viser(dossier, mAnnotPub, mAnnotPriv, mBureauId);
                    }

                    Log.d(LOG_TAG, "CACHET on : " + dossier.getName());
                } catch (IParapheurException e) {
                    Log.e(LOG_TAG, "Error performing task", e);
                    Sentry.capture(e);
                    mErrorMessage = (e.getResId() > 0) ? e.getResId() : R.string.visa_error_message_not_sent_to_server;
                }

                if (isCancelled()) return false;
            }

            // If no error message, then the signature is successful.
            return (mErrorMessage == -1);
        }


        @Override protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);

            if (success) {
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, null);
                dismissAllowingStateLoss();
            }
            else if (getActivity() != null) {
                Toast.makeText(getActivity(), ((mErrorMessage != -1) ? mErrorMessage : R.string.visa_error_message_unknown_error), LENGTH_SHORT).show();
            }
        }

    }

}
