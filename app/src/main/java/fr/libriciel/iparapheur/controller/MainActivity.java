/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import fr.libriciel.iparapheur.R;
import fr.libriciel.iparapheur.controller.account.AccountListFragment;
import fr.libriciel.iparapheur.controller.dossier.DossierDetailFragment;
import fr.libriciel.iparapheur.controller.dossier.action.ArchivageDialogFragment;
import fr.libriciel.iparapheur.controller.dossier.action.MailSecDialogFragment;
import fr.libriciel.iparapheur.controller.dossier.action.RejectDialogFragment;
import fr.libriciel.iparapheur.controller.dossier.action.SealDialogFragment;
import fr.libriciel.iparapheur.controller.dossier.action.SignatureDialogFragment;
import fr.libriciel.iparapheur.controller.dossier.action.TdtActesDialogFragment;
import fr.libriciel.iparapheur.controller.dossier.action.VisaDialogFragment;
import fr.libriciel.iparapheur.controller.preferences.ImportCertificatesDialogFragment;
import fr.libriciel.iparapheur.controller.preferences.PreferencesAccountFragment;
import fr.libriciel.iparapheur.controller.preferences.PreferencesActivity;
import fr.libriciel.iparapheur.model.Account;
import fr.libriciel.iparapheur.model.Action;
import fr.libriciel.iparapheur.model.Bureau;
import fr.libriciel.iparapheur.model.Dossier;
import fr.libriciel.iparapheur.service.rest.api.RestClientFactory;
import fr.libriciel.iparapheur.utils.AccountUtils;
import fr.libriciel.iparapheur.utils.ActionUtils;
import fr.libriciel.iparapheur.utils.CollectionUtils;
import fr.libriciel.iparapheur.utils.FilesUtils;
import fr.libriciel.iparapheur.utils.IParapheurException;
import io.sentry.Sentry;
import io.sentry.android.AndroidSentryClientFactory;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;
import static android.content.res.Configuration.ORIENTATION_PORTRAIT;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static android.view.WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS;
import static android.view.WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
import static android.widget.Toast.LENGTH_SHORT;
import static androidx.core.view.GravityCompat.END;
import static androidx.core.view.GravityCompat.START;
import static androidx.drawerlayout.widget.DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
import static androidx.drawerlayout.widget.DrawerLayout.LOCK_MODE_UNLOCKED;
import static androidx.drawerlayout.widget.DrawerLayout.STATE_SETTLING;
import static fr.libriciel.iparapheur.controller.preferences.PreferencesActivity.ARGUMENT_GO_TO_FRAGMENT;
import static fr.libriciel.iparapheur.controller.preferences.PreferencesActivity.PREFERENCES_ACTIVITY_REQUEST_CODE;
import static fr.libriciel.iparapheur.model.Action.ARCHIVAGE;
import static fr.libriciel.iparapheur.model.Action.CACHET;
import static fr.libriciel.iparapheur.model.Action.MAILSEC;
import static fr.libriciel.iparapheur.model.Action.REJET;
import static fr.libriciel.iparapheur.model.Action.SIGNATURE;
import static fr.libriciel.iparapheur.model.Action.TDT;
import static fr.libriciel.iparapheur.model.Action.TDT_ACTES;
import static fr.libriciel.iparapheur.model.Action.TDT_HELIOS;
import static fr.libriciel.iparapheur.model.Action.VISA;
import static java.util.Arrays.asList;


/**
 * An activity representing a list of Dossiers.
 * The activity presents the list of items and
 * item details side-by-side using two vertical panes.
 * <p/>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link MenuFragment} and the item details
 * is a {@link DossierDetailFragment}.
 * <p/>
 * This activity also implements the required
 * {@link MenuFragment.MenuFragmentListener} interface
 * to listen for item selections.
 */
public class MainActivity extends AppCompatActivity implements MenuFragment.MenuFragmentListener, AccountListFragment.AccountListFragmentListener,
        ActionMode.Callback, DossierDetailFragment.DossierDetailsFragmentListener {

    private static final String LOG_TAG = "MainActivity";
    private static final String SHARED_PREFERENCES_MAIN = ":iparapheur:shared_preferences_main";
    private static final String SHARED_PREFERENCES_IS_DRAWER_KNOWN = "is_drawer_known";
    private static final String SAVED_STATE_SHOULD_SHOW_ACCOUNT_AFTER_ROTATION = "should_show_account_after_rotation";

    private static final String SCHEME_URI_CONTENT = "content";
    private static final String SCHEME_URI_IPARAPHEUR = "iparapheur";
    private static final String SCHEME_URI_IMPORT_CERTIFICATE = "importCertificate";
    private static final String SCHEME_URI_IMPORT_CERTIFICATE_URL = "AndroidUrl";
    private static final @SuppressWarnings("squid:S2068") String SCHEME_URI_IMPORT_CERTIFICATE_PASSWORD = "AndroidPwd";

    private DrawerLayout mLeftDrawerLayout;
    private DrawerLayout mRightDrawerLayout;
    private FrameLayout mLeftDrawerMenu;
    private ActionBarDrawerToggle mLeftDrawerToggle;
    private ViewSwitcher mNavigationDrawerAccountViewSwitcher;

    private boolean mShouldShowAccountAfterRotation = false;
    private ActionMode mActionMode;                            // The actionMode used when dossiers are checked


    // <editor-fold desc="LifeCycle">


    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Context ctx = this.getApplicationContext();
        Sentry.init("https://f4de6f6e80e74c4690dad83616dd4878@sentry.libriciel.fr/6", new AndroidSentryClientFactory(ctx));

        // To have a transparent StatusBar, and a background color behind

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        // Loading indicator

        setContentView(R.layout.main_activity);

        mLeftDrawerLayout = findViewById(R.id.activity_dossiers_drawer_layout);
        mRightDrawerLayout = findViewById(R.id.activity_dossiers_right_drawer_layout);
        mLeftDrawerMenu = findViewById(R.id.activity_dossiers_left_drawer);

        Toolbar toolbar = findViewById(R.id.menu_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Drawers

        mLeftDrawerToggle = new DossiersActionBarDrawerToggle(this, mLeftDrawerLayout);
        mLeftDrawerLayout.addDrawerListener(mLeftDrawerToggle);
        mRightDrawerLayout.setDrawerLockMode(LOCK_MODE_LOCKED_CLOSED);

        View unCastedRootView = findViewById(R.id.root_content);
        if (unCastedRootView instanceof ViewSwitcher)
            mNavigationDrawerAccountViewSwitcher = (ViewSwitcher) unCastedRootView;

        ImageButton drawerAccountImageButton = findViewById(R.id.navigation_drawer_menu_header_account_button);
        if (drawerAccountImageButton != null) {
            drawerAccountImageButton.setOnClickListener(v -> {

                if (mNavigationDrawerAccountViewSwitcher == null)
                    return;

                boolean switchToAccountView = (mNavigationDrawerAccountViewSwitcher.getDisplayedChild() == 0);
                if (switchToAccountView)
                    mNavigationDrawerAccountViewSwitcher.setDisplayedChild(1);
                else
                    mNavigationDrawerAccountViewSwitcher.setDisplayedChild(0);

                View filterButton = findViewById(R.id.navigation_drawer_filters_menu_header_filters_imagebutton);
                View downloadButton = findViewById(R.id.navigation_drawer_filters_menu_header_download_imagebutton);

                filterButton.setVisibility(switchToAccountView ? GONE : VISIBLE);
                downloadButton.setVisibility(switchToAccountView ? GONE : VISIBLE);
            });
        }

        // ContentView Fragment restore

        Fragment contentFragment = getSupportFragmentManager().findFragmentByTag(DossierDetailFragment.FRAGMENT_TAG);
        if (contentFragment == null) {
            contentFragment = new DossierDetailFragment();
        }

        contentFragment.setRetainInstance(true);

        FragmentTransaction contentTransaction = getSupportFragmentManager().beginTransaction();
        contentTransaction.replace(R.id.dossier_detail_layout, contentFragment, DossierDetailFragment.FRAGMENT_TAG);
        contentTransaction.commit();
    }


    @Override protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Sync the toggle state after onRestoreInstanceState has occurred.

        mLeftDrawerToggle.syncState();
        refreshNavigationDrawerHeader();

        // Default FAB visibility

        View fabSwitcher = findViewById(R.id.mupdf_main_fab_viewswitcher);
        FloatingActionButton mainFab = findViewById(R.id.mupdf_main_menu_fabbutton);

        if (fabSwitcher != null)
            fabSwitcher.setVisibility(GONE);

        if (mainFab != null)
            mainFab.hide();

        //

        if (savedInstanceState != null)
            mShouldShowAccountAfterRotation = savedInstanceState.getBoolean(SAVED_STATE_SHOULD_SHOW_ACCOUNT_AFTER_ROTATION);
    }


    @Override protected void onStart() {
        super.onStart();

        // Check possible Scheme URI call.
        // Waiting arguments like :
        //
        // iparapheur://importCertificate?AndroidUrl=https%3A%2F%2Fcurl.adullact.org%2FsC4VU%2Fbma.p12      (mandatory)
        //                               &AndroidPwd=bma                                                    (optional)
        //                               &iOsUrl=https%3A%2F%2Fcurl.adullact.org%2FSUZI2%2Fbma.p12          (ignored)
        //                               &iOsPwd=bma                                                        (ignored)
        Uri schemeUri = getIntent().getData();
        boolean isValidUriScheme = (schemeUri != null) && TextUtils.equals(schemeUri.getScheme(), SCHEME_URI_IPARAPHEUR);
        boolean isIparapheurImportCert = (schemeUri != null) && TextUtils.equals(schemeUri.getHost(), SCHEME_URI_IMPORT_CERTIFICATE);

        if (isValidUriScheme && isIparapheurImportCert) {

            String certificateUrl = schemeUri.getQueryParameter(SCHEME_URI_IMPORT_CERTIFICATE_URL);
            String certificatePassword = schemeUri.getQueryParameter(SCHEME_URI_IMPORT_CERTIFICATE_PASSWORD);

            if (!TextUtils.isEmpty(certificateUrl))
                importCertificate(certificateUrl, certificatePassword);
            else
                Toast.makeText(this, R.string.import_error_message_incorrect_scheme, LENGTH_SHORT).show();
        }

        // Check if there is an import by "Open with"

        boolean isImportFileContentType = TextUtils.equals(getIntent().getScheme(), SCHEME_URI_CONTENT);
        boolean isImportFileOctetStream = TextUtils.equals(getIntent().getType(), "application/octet-stream");
        Log.e(LOG_TAG, "INTENT : " + getIntent());

        if (isImportFileContentType && isImportFileOctetStream) {
            try (InputStream is = getContentResolver().openInputStream(getIntent().getData())) {

                File tempFile = File.createTempFile("temp_" + SystemClock.uptimeMillis(), ".bks", getCacheDir());
                FileUtils.copyInputStreamToFile(is, tempFile);
                tempFile.deleteOnExit();

                DialogFragment actionDialog = ImportCertificatesDialogFragment.newInstance(tempFile);
                actionDialog.show(getSupportFragmentManager(), ImportCertificatesDialogFragment.FRAGMENT_TAG);

            } catch (IOException e) {
                Log.e(LOG_TAG, "Cannot read intent", e);
                e.printStackTrace();
            }
        }

        // If we let the intent data, it can try to re-import certificate, back from history.
        getIntent().setType(null);
        getIntent().setData(null);
    }


    @Override public void onResume() {
        super.onResume();

        // On first launch, we have to open the NavigationDrawer.
        // It's in the Android guidelines, the user have to know it's here.
        // (And we want to open it in portrait in any case, otherwise the user sees a weird grey panel)

        SharedPreferences settings = getSharedPreferences(SHARED_PREFERENCES_MAIN, 0);
        boolean isDrawerKnown = settings.getBoolean(SHARED_PREFERENCES_IS_DRAWER_KNOWN, false);
        boolean isDeviceInPortrait = (getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT);

        if (!isDrawerKnown) {
            mLeftDrawerLayout.openDrawer(mLeftDrawerMenu);

            // Registering the fact that the user knows the drawer
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean(SHARED_PREFERENCES_IS_DRAWER_KNOWN, true);
            editor.apply();
        }
        else if (mShouldShowAccountAfterRotation) {

            mShouldShowAccountAfterRotation = false;
            mLeftDrawerLayout.openDrawer(mLeftDrawerMenu);

            if (isDeviceInPortrait && (mNavigationDrawerAccountViewSwitcher != null)) {
                mNavigationDrawerAccountViewSwitcher.setDisplayedChild(1);
            }
        }
        else {
            mLeftDrawerLayout.closeDrawer(mLeftDrawerMenu);
        }

        // Restoring proper Drawer state on selected dossiers

        MenuFragment menuFragment = (MenuFragment) getSupportFragmentManager().findFragmentByTag(MenuFragment.FRAGMENT_TAG);
        if ((menuFragment != null) && !menuFragment.getCheckedDossiers().isEmpty()) {
            mActionMode = startSupportActionMode(this);

            if (isDeviceInPortrait)
                mLeftDrawerLayout.openDrawer(mLeftDrawerMenu);
            else
                mLeftDrawerLayout.closeDrawer(mLeftDrawerMenu);
        }
    }


    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PREFERENCES_ACTIVITY_REQUEST_CODE) {

            // Notify BureauxFragments to update accounts list (the bureau will update back this Activity if needed)
            AccountListFragment accountListFragment = (AccountListFragment) getSupportFragmentManager()
                    .findFragmentByTag(AccountListFragment.FRAGMENT_TAG);

            if (accountListFragment != null) {
                accountListFragment.accountsChanged();
            }
        }
    }


    @SuppressWarnings("squid:S3776")
    @Override public void onBackPressed() {

        if (getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT) {

            // Drawer account back

            if (mNavigationDrawerAccountViewSwitcher.getDisplayedChild() == 1) {
                mNavigationDrawerAccountViewSwitcher.setDisplayedChild(0);
                return;
            }

            if (mLeftDrawerLayout.isDrawerOpen(mLeftDrawerMenu)) {

                // Menu back

                MenuFragment bureauxFragment = (MenuFragment) getSupportFragmentManager().findFragmentByTag(MenuFragment.FRAGMENT_TAG);
                if (bureauxFragment != null)
                    if (bureauxFragment.onBackPressed())
                        return;

                // Close the drawer

                if (mLeftDrawerLayout.isDrawerOpen(mLeftDrawerMenu)) {
                    mLeftDrawerLayout.closeDrawer(mLeftDrawerMenu);
                    return;
                }

            }
            else {

                // Collapse the FAB

                DossierDetailFragment dossierDetailFragment = (DossierDetailFragment) getSupportFragmentManager()
                        .findFragmentByTag(DossierDetailFragment.FRAGMENT_TAG);
                if ((dossierDetailFragment != null) && dossierDetailFragment.onBackPressed()) return;

                // Menu back

                MenuFragment bureauxFragment = (MenuFragment) getSupportFragmentManager().findFragmentByTag(MenuFragment.FRAGMENT_TAG);
                if (bureauxFragment != null) {
                    if (bureauxFragment.onBackPressed()) {
                        mLeftDrawerLayout.openDrawer(mLeftDrawerMenu);
                        return;
                    }
                }

            }
        }
        else {

            // First, close the drawer

            if (mLeftDrawerLayout.isDrawerOpen(mLeftDrawerMenu)) {
                mLeftDrawerLayout.closeDrawer(mLeftDrawerMenu);
                return;
            }

            // Collapse the FAB

            DossierDetailFragment dossierDetailFragment = (DossierDetailFragment) getSupportFragmentManager()
                    .findFragmentByTag(DossierDetailFragment.FRAGMENT_TAG);
            if (dossierDetailFragment != null)
                if (dossierDetailFragment.onBackPressed())
                    return;

            // Then, try to pop backstack

            MenuFragment bureauxFragment = (MenuFragment) getSupportFragmentManager()
                    .findFragmentByTag(MenuFragment.FRAGMENT_TAG);

            if ((bureauxFragment != null) && (bureauxFragment.onBackPressed())) {
                return;
            }
        }

        super.onBackPressed();
    }


    @Override protected void onSaveInstanceState(Bundle outState) {

        boolean isDrawerOpened = mLeftDrawerLayout.isDrawerOpen(mLeftDrawerMenu);
        boolean isInLandscape = mNavigationDrawerAccountViewSwitcher == null;
        boolean isAccountViewSelected = (mNavigationDrawerAccountViewSwitcher != null)
                                        && (mNavigationDrawerAccountViewSwitcher.getDisplayedChild() == 1);
        boolean isAccountShown = isDrawerOpened && (isInLandscape || isAccountViewSelected);

        outState.putBoolean(SAVED_STATE_SHOULD_SHOW_ACCOUNT_AFTER_ROTATION, isAccountShown);

        super.onSaveInstanceState(outState);
    }


    // </editor-fold desc="LifeCycle">


    // <editor-fold desc="ActionBar">


    @Override public boolean onCreateOptionsMenu(Menu menu) {

        Toolbar actionsToolbar = findViewById(R.id.actions_toolbar);
        if (actionsToolbar != null) {

            actionsToolbar.getMenu().clear();
            actionsToolbar.inflateMenu(R.menu.main_activity);
            actionsToolbar.setOnMenuItemClickListener(this::onOptionsItemSelected);
        }

        return super.onCreateOptionsMenu(menu);
    }


    @Override public boolean onPrepareOptionsMenu(Menu menu) {
        return false;
    }


    @Override public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event

        if (mLeftDrawerToggle.onOptionsItemSelected(item)) return true;

        // TODO : handle dossier(s) actions
        // Handle presses on the action bar items

        if (item.getItemId() == R.id.action_settings) {
            startActivityForResult(new Intent(this, PreferencesActivity.class), PREFERENCES_ACTIVITY_REQUEST_CODE);
            return true;
        }
        else {
            return super.onOptionsItemSelected(item);
        }
    }


    // </editor-fold desc="ActionBar">


    // <editor-fold desc="ActionMode">


    @Override public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        actionMode.setTitleOptionalHint(true);
        MenuInflater inflater = actionMode.getMenuInflater();
        inflater.inflate(R.menu.dossier_details_fragment_actions, menu);

        Window window = getWindow();
        window.addFlags(FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.contextual_700));

        return true;
    }


    @Override public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {

        MenuFragment fragment = (MenuFragment) getSupportFragmentManager().findFragmentByTag(MenuFragment.FRAGMENT_TAG);

        // Default cases

        if (fragment == null)
            return false;

        HashSet<Dossier> checkedDossiers = fragment.getCheckedDossiers();

        if ((checkedDossiers == null) || (checkedDossiers.isEmpty())) {
            actionMode.finish();
            return false;
        }

        // Get available actions from Dossiers

        HashSet<Action> actions = new HashSet<>(asList(Action.values()));

        HashSet<Action> availableActions = new HashSet<>();
        availableActions.add(ActionUtils.computePositiveAction(checkedDossiers));
        availableActions.add(ActionUtils.computeNegativeAction(checkedDossiers));
        availableActions.addAll(ActionUtils.computeSecondaryActions(checkedDossiers));

        // Compute visibility

        actionMode.setTitle(String.format(getString(R.string.action_mode_nb_dossiers), checkedDossiers.size()));
        menu.setGroupVisible(R.id.dossiers_menu_main_actions, false);
        menu.setGroupVisible(R.id.dossiers_menu_other_actions, false);

        for (Action action : actions) {

//			MenuItem item;
//			int menuItemId;
//			String menuTitle;
//
//			// Mixed sets
//
//			if (action.equals(Action.VISA) && hasCachet) {
//				menuItemId = Action.SIGNATURE.getMenuItemId();
//				menuTitle = getString(Action.VISA.getTitle()) + "/" + getString(Action.CACHET.getTitle());
//			}
//			else if (action.equals(Action.VISA) && hasSignature) {
//				menuItemId = Action.SIGNATURE.getMenuItemId();
//				menuTitle = getString(Action.VISA.getTitle()) + "/" + getString(Action.SIGNATURE.getTitle());
//			}
//			else if (action.equals(Action.CACHET) && hasSignature) {
//				menuItemId = Action.SIGNATURE.getMenuItemId();
//				menuTitle = getString(Action.CACHET.getTitle()) + "/" + getString(Action.SIGNATURE.getTitle());
//			}
//			else {
//				menuItemId = action.getMenuItemId();
//				menuTitle = getString(action.getTitle());
//			}
//
//			// If we only have signPapier type, we only have a VISA, actually
//			boolean isVisible = !(action.equals(Action.SIGNATURE) && !hasSignature);
//
            // Set current state

            MenuItem item = menu.findItem(action.getMenuItemId());

            if (item != null) {
//				item.setTitle(menuTitle);
                item.setVisible(availableActions.contains(action));
            }
        }

        return true;
    }


    @Override public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {

        MenuFragment menuFragment = (MenuFragment) getSupportFragmentManager().findFragmentByTag(MenuFragment.FRAGMENT_TAG);
        if (menuFragment == null)
            return false;

        Bureau bureau = menuFragment.getSelectedBureau();
        Action invokedAction = Action.fromId(menuItem.getItemId());

        if ((invokedAction != null) && (bureau != null)) {
            launchActionPopup(menuFragment.getCheckedDossiers(), bureau.getId(), invokedAction);
        }

        return true;
    }


    @Override public void onDestroyActionMode(ActionMode actionMode) {

        MenuFragment menuFragment = (MenuFragment) getSupportFragmentManager().findFragmentByTag(MenuFragment.FRAGMENT_TAG);
        if (menuFragment != null) {
            menuFragment.clearCheckSelection();
        }

        Window window = getWindow();
        window.addFlags(FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(this, android.R.color.transparent));

        mActionMode = null;
    }


    // </editor-fold desc="ActionMode">


    private void refreshNavigationDrawerHeader() {

        Account account = AccountUtils.SELECTED_ACCOUNT;

        TextView navigationDrawerAccountTitle = findViewById(R.id.navigation_drawer_menu_header_title);
        TextView navigationDrawerAccountSubTitle = findViewById(R.id.navigation_drawer_menu_header_subtitle);

        if (navigationDrawerAccountTitle != null)
            navigationDrawerAccountTitle.setText(account.getTitle());

        if (navigationDrawerAccountSubTitle != null)
            navigationDrawerAccountSubTitle.setText(account.getLogin());

    }


    private void importCertificate(@NonNull final String url, @Nullable final String password) {

        String certificateFileName = url.substring(url.lastIndexOf('/') + 1);
        final String certificateLocalPath = new File(getExternalCacheDir(), certificateFileName).getAbsolutePath();

        Executor executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());
        AtomicInteger errorMessageResourceAtomic = new AtomicInteger(-1);

        executor.execute(() -> {

            try {
                boolean downloadSuccessful = new RestClientFactory()
                        .createRestClient(AccountUtils.SELECTED_ACCOUNT, this)
                        .downloadCertificate(url, certificateLocalPath);

                if (!downloadSuccessful) {
                    errorMessageResourceAtomic.set(R.string.import_error_message_cant_download_certificate);
                }

            } catch (IParapheurException e) {
                Sentry.capture(e);
                Log.e(LOG_TAG, "Error downloading certificate", e);
            }

            handler.post(() -> {

                if (errorMessageResourceAtomic.get() == -1) {

                    if (password != null) {
                        FilesUtils.importCertificate(MainActivity.this, new File(certificateLocalPath), password);
                    }
                    else {
                        DialogFragment actionDialog = ImportCertificatesDialogFragment.newInstance(new File(certificateLocalPath));
                        actionDialog.show(getSupportFragmentManager(), ImportCertificatesDialogFragment.FRAGMENT_TAG);
                    }
                }
                else {
                    Toast.makeText(MainActivity.this, errorMessageResourceAtomic.get(), LENGTH_SHORT).show();
                }
            });
        });
    }


    private void launchActionPopup(@NonNull Set<Dossier> dossierSet, @NonNull String bureauId, @NonNull Action action) {

        MenuFragment menuFragment = (MenuFragment) getSupportFragmentManager().findFragmentByTag(MenuFragment.FRAGMENT_TAG);
        DialogFragment actionDialog;
        ArrayList<Dossier> dossierList = new ArrayList<>(dossierSet);

        if (action == REJET) {
            actionDialog = RejectDialogFragment.newInstance(dossierList, bureauId);
            actionDialog.setTargetFragment(menuFragment, RejectDialogFragment.REQUEST_CODE_REJECT);
            actionDialog.show(getSupportFragmentManager(), RejectDialogFragment.FRAGMENT_TAG);
        }
        else if (action == VISA) {
            actionDialog = VisaDialogFragment.newInstance(dossierList, bureauId);
            actionDialog.setTargetFragment(menuFragment, VisaDialogFragment.REQUEST_CODE_VISA);
            actionDialog.show(getSupportFragmentManager(), VisaDialogFragment.FRAGMENT_TAG);
        }
        else if (action == CACHET) {
            actionDialog = SealDialogFragment.newInstance(dossierList, bureauId);
            actionDialog.setTargetFragment(menuFragment, SealDialogFragment.REQUEST_CODE_SEAL);
            actionDialog.show(getSupportFragmentManager(), SealDialogFragment.FRAGMENT_TAG);
        }
        else if (action == SIGNATURE) {
            actionDialog = SignatureDialogFragment.newInstance(dossierList, bureauId);
            actionDialog.setTargetFragment(menuFragment, SignatureDialogFragment.REQUEST_CODE_SIGNATURE);
            actionDialog.show(getSupportFragmentManager(), SignatureDialogFragment.FRAGMENT_TAG);
        }
        else if (action == MAILSEC) {
            actionDialog = MailSecDialogFragment.newInstance(dossierList, bureauId);
            actionDialog.show(getSupportFragmentManager(), "MailSecDialogFragment");
        }
        else if ((action == TDT) || (action == TDT_HELIOS) || (action == TDT_ACTES)) {
            actionDialog = TdtActesDialogFragment.newInstance(dossierList, bureauId);
            actionDialog.setTargetFragment(menuFragment, TdtActesDialogFragment.REQUEST_CODE_ACTES);
            actionDialog.show(getSupportFragmentManager(), TdtActesDialogFragment.FRAGMENT_TAG);
        }
        else if (action == ARCHIVAGE) {
            actionDialog = ArchivageDialogFragment.newInstance(dossierList, bureauId);
            actionDialog.show(getSupportFragmentManager(), "ArchivageDialogFragment");
        }
        else {
            Log.e(LOG_TAG, "UNKNOWN ACTION : " + action);
        }
    }


    // <editor-fold desc="AccountFragmentListener">


    @Override public void onAccountSelected(@NonNull Account account) {

        AccountUtils.SELECTED_ACCOUNT = account;
        refreshNavigationDrawerHeader();

        // Close the drawer

        if (getResources().getConfiguration().orientation == ORIENTATION_LANDSCAPE)
            mLeftDrawerLayout.closeDrawer(mLeftDrawerMenu);
        else if (mNavigationDrawerAccountViewSwitcher != null)
            mNavigationDrawerAccountViewSwitcher.setDisplayedChild(0);

        // If we selected a new account, and we have a Dossier list displayed
        // We'll want to pop the BackStack to get on the Bureau list
        // Then , we just update the Bureau with the accurate Account

        MenuFragment menuFragment = (MenuFragment) getSupportFragmentManager().findFragmentByTag(MenuFragment.FRAGMENT_TAG);
        if (menuFragment != null) {
            menuFragment.onBackPressed();
            menuFragment.updateDesks(true);
        }
    }


    @Override public void onCreateAccountInvoked() {
        Intent preferencesIntent = new Intent(this, PreferencesActivity.class);
        preferencesIntent.putExtra(ARGUMENT_GO_TO_FRAGMENT, PreferencesAccountFragment.class.getSimpleName());
        startActivityForResult(preferencesIntent, PREFERENCES_ACTIVITY_REQUEST_CODE);
    }


    // </editor-fold desc="AccountFragmentListener">


    // <editor-fold desc="MenuFragment">


    @Override public void onDossierListFragmentSelected(@NonNull Dossier dossier, @NonNull String bureauId) {

        if (mLeftDrawerLayout.isDrawerOpen(mLeftDrawerMenu))
            mLeftDrawerLayout.closeDrawer(mLeftDrawerMenu);

        DossierDetailFragment fragment = (DossierDetailFragment) getSupportFragmentManager().findFragmentByTag(DossierDetailFragment.FRAGMENT_TAG);
        if ((fragment != null)) {
            fragment.showProgressLayout();
            fragment.update(dossier, bureauId, null);
        }
    }


    @Override public void onDossierCheckedChanged(boolean forceClose) {

        if (mActionMode != null)
            mActionMode.invalidate();
        else if (!forceClose)
            mActionMode = startSupportActionMode(this);
    }


    // </editor-fold desc="MenuFragment">


    // <editor-fold desc="DossierDetailsFragmentListener">


    @Override public boolean isAnyDrawerOpened() {
        return (mRightDrawerLayout.isDrawerVisible(END) || mLeftDrawerLayout.isDrawerVisible(START));
    }


    @Override public void toggleInfoDrawer() {

        if (mRightDrawerLayout.isDrawerOpen(END))
            mRightDrawerLayout.closeDrawer(END);
        else
            mRightDrawerLayout.openDrawer(END);
    }


    @Override public void lockInfoDrawer(boolean lock) {

        if (lock) {
            mRightDrawerLayout.closeDrawer(END);
            mRightDrawerLayout.setDrawerLockMode(LOCK_MODE_LOCKED_CLOSED);
        }
        else {
            mRightDrawerLayout.setDrawerLockMode(LOCK_MODE_UNLOCKED);
        }
    }


    @Override public void onActionButtonClicked(@NonNull Dossier dossier, @NonNull String bureauId, @NonNull Action action) {
        launchActionPopup(CollectionUtils.asSet(dossier), bureauId, action);
    }


    // </editor-fold desc="DossierDetailsFragmentListener">


    /**
     * Listener used on the Drawer Layout used to control the Action Bar content depending
     * on the Drawer state.
     */
    private class DossiersActionBarDrawerToggle extends ActionBarDrawerToggle {

        private DossiersActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout) {
            super(activity, drawerLayout, activity.findViewById(R.id.menu_toolbar), R.string.drawer_open, R.string.drawer_close);
        }


        @Override public void onDrawerClosed(View view) {

            if (getSupportActionBar() != null) {
                Account selectedAccount = AccountUtils.SELECTED_ACCOUNT;
                if (selectedAccount != null) getSupportActionBar().setTitle(selectedAccount.getTitle());
            }

            if (mNavigationDrawerAccountViewSwitcher != null)
                mNavigationDrawerAccountViewSwitcher.setDisplayedChild(0);

            // calls onPrepareOptionMenu to show context specific actions
            invalidateOptionsMenu();
        }


        @Override public void onDrawerOpened(View drawerView) {
            if (getSupportActionBar() != null)
                getSupportActionBar().setTitle(R.string.app_name);

            // calls onPrepareOptionMenu to hide context specific actions
            invalidateOptionsMenu();
        }


        @Override public void onDrawerStateChanged(int newState) {

            if (newState == STATE_SETTLING)
                if (mActionMode != null)
                    mActionMode.finish();

            invalidateOptionsMenu();
        }

    }

}
