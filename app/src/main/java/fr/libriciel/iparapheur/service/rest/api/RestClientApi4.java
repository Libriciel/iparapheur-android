/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.service.rest.api;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import fr.libriciel.iparapheur.R;
import fr.libriciel.iparapheur.model.Account;
import fr.libriciel.iparapheur.model.Annotation;
import fr.libriciel.iparapheur.model.Bureau;
import fr.libriciel.iparapheur.model.Circuit;
import fr.libriciel.iparapheur.model.DataToSign;
import fr.libriciel.iparapheur.model.DataToSignLegacy;
import fr.libriciel.iparapheur.model.Dossier;
import fr.libriciel.iparapheur.model.Filter;
import fr.libriciel.iparapheur.model.PageAnnotations;
import fr.libriciel.iparapheur.model.ParapheurType;
import fr.libriciel.iparapheur.model.RequestResponse;
import fr.libriciel.iparapheur.model.SignInfo;
import fr.libriciel.iparapheur.model.State;
import fr.libriciel.iparapheur.service.rest.RestUtils;
import fr.libriciel.iparapheur.service.rest.mapper.ModelMapper;
import fr.libriciel.iparapheur.service.rest.request.DataToSignRequest;
import fr.libriciel.iparapheur.service.rest.request.Signature46Request;
import fr.libriciel.iparapheur.service.rest.request.GetSignInfoRequest;
import fr.libriciel.iparapheur.service.rest.request.RemoteDocument;
import fr.libriciel.iparapheur.service.rest.request.Signature47Request;
import fr.libriciel.iparapheur.service.rest.request.SignatureResult;
import fr.libriciel.iparapheur.utils.CollectionUtils;
import fr.libriciel.iparapheur.utils.IParapheurException;
import fr.libriciel.iparapheur.utils.JacksonRequest;
import fr.libriciel.iparapheur.utils.JsonExplorer;
import fr.libriciel.iparapheur.utils.SerializableSparseArray;
import fr.libriciel.iparapheur.utils.StringsUtils;
import io.sentry.Sentry;
import lombok.RequiredArgsConstructor;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;
import static com.fasterxml.jackson.databind.DeserializationFeature.UNWRAP_ROOT_VALUE;
import static fr.libriciel.iparapheur.service.rest.request.DataToSignRequest.DIGEST_ALGORITHM_SHA1;
import static fr.libriciel.iparapheur.service.rest.request.DataToSignRequest.DIGEST_ALGORITHM_SHA256;
import static java.net.HttpURLConnection.HTTP_FORBIDDEN;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static java.net.HttpURLConnection.HTTP_NOT_FOUND;
import static java.net.HttpURLConnection.HTTP_OK;


@SuppressLint("DefaultLocale")
@RequiredArgsConstructor
public class RestClientApi4 implements RestClientInterface {

    private static final String ACTION_LOGIN = "/parapheur/api/login";
    private static final String LOG_TAG = "RestClientApi";

    private static final String HTTPS = "https";
    private static final String PATH_PARAPHEUR = "parapheur";
    private static final String PATH_CRYPTO = "crypto";
    private static final String PARAM_ALF_TICKET = "alf_ticket";
    private static final String PARAM_CURRENT_DESK = "bureauCourant";
    private static final String PARAM_PUBLIC_ANNOTATION = "annotPub";
    private static final String PARAM_PRIVATE_ANNOTATION = "annotPriv";

    public static final long SESSION_TIMEOUT = 30 * 60 * 1000L;

    private static final String RESOURCE_DOSSIERS = "/parapheur/dossiers";

    /* Ressources principales */
    private static final String RESOURCE_BUREAUX = "/parapheur/bureaux";
    private static final String RESOURCE_DOSSIER_CIRCUIT = "/parapheur/dossiers/%s/circuit";
    private static final String RESOURCE_TYPES = "/parapheur/types";
    private static final String RESOURCE_ANNOTATIONS = "/parapheur/dossiers/%s/%s/annotations";
    private static final String RESOURCE_ANNOTATION = "/parapheur/dossiers/%s/%s/annotations/%s";
    private static final String RESOURCE_USER_INFO = "/webframework/content/metadata2";
    private static final String USER_INFO_FIRST_NAME = "{http://www.alfresco.org/model/content/1.0}firstName";
    private static final String USER_INFO_LAST_NAME = "{http://www.alfresco.org/model/content/1.0}lastName";
    private static final String ACTION_VISA = "/parapheur/dossiers/%s/visa";
    private static final String ACTION_SIGNATURE = "/parapheur/dossiers/%s/signature";
    private static final String ACTION_SIGNATURE_PAPIER = "/parapheur/dossiers/%s/signPapier";
    private static final String ACTION_TDT_ACTES = "/parapheur/dossiers/%s/tdtActes";
    private static final String ACTION_TDT_HELIOS = "/parapheur/dossiers/%s/tdtHelios";
    private static final String ACTION_MAILSEC = "/parapheur/dossiers/%s/mailsec";
    private static final String ACTION_REJET = "/parapheur/dossiers/%s/rejet";
    private static final String ACTION_SEAL = "/parapheur/dossiers/%s/seal";

    private final Gson mGson = CollectionUtils.buildGsonWithDateParser();
    private final ModelMapper modelMapper = new ModelMapper();
    private final Account account;
    private final Context context;


    private String getAuthority() {
        return TextUtils.isEmpty(account.getTenant())
               ? "m-" + account.getServerBaseUrl()
               : "m-" + account.getTenant() + "." + account.getServerBaseUrl();
    }


    @Override public int test() throws IParapheurException {

        // Build request

        String requestContent = RestUtils.getAuthenticationJsonData(account);
        String requestUrl = buildUrl(ACTION_LOGIN, null, false);

        RequestResponse response = RestUtils.post(requestUrl, requestContent);

        // Parse response

        @StringRes int messageRes = R.string.http_error_undefined;

        if (response == null) {
            messageRes = R.string.http_error_undefined;
        }
        else if (response.getCode() == HTTP_OK) {
            messageRes = R.string.test_ok;
        }
        else if (response.getCode() == HTTP_FORBIDDEN) {
            messageRes = R.string.test_forbidden;
        }
        else if (response.getCode() == HTTP_NOT_FOUND) {
            messageRes = R.string.test_not_found;
        }
        else if ((response.getCode() == HTTP_INTERNAL_ERROR) && response.getError().contains("Tenant does not exist")) {
            messageRes = R.string.test_tenant_not_exist;
        }

        return messageRes;
    }


    public String getTicket(Account account) throws IParapheurException {

        // Default case

        if (RestUtils.hasValidTicket(account)) return account.getTicket();

        // Building request

        String requestContent = RestUtils.getAuthenticationJsonData(account);
        String requestUrl = buildUrl(ACTION_LOGIN, null, false);

        RequestResponse response = RestUtils.post(requestUrl, requestContent);

        // Parsing response

        if (response != null) {

            String responseTicket = new JsonExplorer(response.getResponse()).findObject("data").optString("ticket");
            if (TextUtils.isEmpty(responseTicket))
                throw new IParapheurException(R.string.error_parse, null);

            account.setTicket(responseTicket);
        }

        return account.getTicket();
    }


    public @NonNull String buildUrl(@NonNull String action) throws IParapheurException {
        return buildUrl(action, null, true);
    }


    public @NonNull String buildUrl(@NonNull String action, @Nullable String params, boolean withTicket) throws IParapheurException {
        return buildUrl(action, params, withTicket, true);
    }


    public @NonNull String buildUrl(@NonNull String action, @Nullable String params, boolean withTicket, boolean withTenant)
            throws IParapheurException {

        // Default checks

        if (account == null) throw new IParapheurException(R.string.error_no_account, null);

        String ticket = null;
        if (withTicket) ticket = getTicket(account);

        if (withTicket && TextUtils.isEmpty(ticket)) throw new IParapheurException(R.string.error_no_ticket, null);

        account.setLastRequest(new Date());

        // Build URL

        StringBuilder stringBuilder = new StringBuilder(BASE_PATH);

        if (withTenant && (!TextUtils.isEmpty(account.getTenant()))) stringBuilder.append(account.getTenant()).append(".");

        stringBuilder.append(account.getServerBaseUrl());
        stringBuilder.append(action);

        if (withTicket) stringBuilder.append("?").append(PARAM_ALF_TICKET).append("=").append(ticket);

        if (!TextUtils.isEmpty(params))
            stringBuilder.append("&").append(params);

        //

        return stringBuilder.toString();
    }


    @Override public boolean downloadFile(@NonNull String url, @NonNull String path) throws IParapheurException {

        String state = Environment.getExternalStorageState();

        if (!Environment.MEDIA_MOUNTED.equals(state)) throw new IParapheurException(R.string.error_no_storage, null);

        File file = new File(path);
        String fullUrl = buildUrl(url);
        FileOutputStream fileOutput = null;

        try {
            InputStream response = RestUtils.downloadFile(fullUrl);
            fileOutput = new FileOutputStream(file);
            byte[] buffer = new byte[1024];
            int bufferLength;

            while ((bufferLength = response.read(buffer)) > 0) fileOutput.write(buffer, 0, bufferLength);

            // Close the output stream when done
            fileOutput.close();
        } catch (FileNotFoundException e) {
            Sentry.capture(e);
            throw new IParapheurException(R.string.error_file_not_found, null);
        } catch (IOException e) {
            Sentry.capture(e);
            throw new IParapheurException(R.string.error_parse, null);
        } finally {
            if (fileOutput != null) {
                try {
                    fileOutput.close();
                } catch (IOException logOrIgnore) {
                    Log.e(LOG_TAG, "Error", logOrIgnore);
                }
            }
        }

        return file.exists();
    }


    @Override public boolean downloadCertificate(@NonNull String urlString, @NonNull String certificateLocalPath) throws IParapheurException {

        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;

        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HTTP_OK) {
                throw new IParapheurException(R.string.import_certificate, "" + connection.getResponseCode() + " : " + connection.getResponseMessage());
            }

            // download the file
            input = connection.getInputStream();
            output = new FileOutputStream(certificateLocalPath);

            byte[] data = new byte[4096];
            int count;

            while ((count = input.read(data)) != -1) output.write(data, 0, count);
        } catch (IOException e) {
            Sentry.capture(e);
            Log.e(LOG_TAG, "Error", e);
            throw new IParapheurException(R.string.import_error_message_cant_download_certificate, e.getLocalizedMessage());
        } finally {

            try {
                if (output != null) output.close();

                if (input != null) input.close();
            } catch (IOException ignored) { }

            if (connection != null) connection.disconnect();
        }

        return new File(certificateLocalPath).exists();
    }


    @Override public Dossier getFolder(String bureauId, String dossierId) throws IParapheurException {

        String url = buildUrl(RESOURCE_DOSSIERS + "/" + dossierId, PARAM_CURRENT_DESK + "=" + bureauId, true);
        String response = RestUtils.get(url).getResponse().toString();

        return Dossier.fromJsonObject(response, mGson);
    }


    @Override public List<Dossier> getFolders(@NonNull String bureauId, @Nullable Filter filter) throws IParapheurException {

        String params = "asc=true"
                        + "&bureau=" + bureauId
                        + "&corbeilleName=" + (((filter != null)
                                                ? filter.getState().getServerValue()
                                                : State.A_TRAITER.getServerValue()))
                        + ((filter != null)
                           ? "&filter=" + filter.getJSONFilter()
                           : "")
                        + "&metas={}"
                        + "&page=0"
                        + "&pageSize=25"
                        + "&pendingFile=0"
                        + "&skipped=0"
                        + "&sort=cm:created";

        //Log.d( IParapheurHttpClient.class, "REQUEST on " + FOLDERS_PATH + ": " + requestBody );

        String url = buildUrl(RESOURCE_DOSSIERS, params, true);
        String response = RestUtils.get(url).getResponseArray().toString();

        return Dossier.fromJsonArray(response, mGson);
    }


    @Override public List<Bureau> getDesks() throws IParapheurException {

        String url = buildUrl(RESOURCE_BUREAUX);
        RequestResponse result = RestUtils.get(url);

        return Bureau.fromJsonArray(result.getResponseArray().toString(), mGson);
    }


    @Override public List<ParapheurType> getTypology() throws IParapheurException {

        String url = buildUrl(RESOURCE_TYPES);
        RequestResponse result = RestUtils.get(url);

        return ParapheurType.fromJsonArray(result.getResponseArray().toString(), mGson);
    }


    @Override public Circuit getWorkflow(String dossierId) throws IParapheurException {

        String url = buildUrl(String.format(RESOURCE_DOSSIER_CIRCUIT, dossierId));
        String response = String.valueOf(RestUtils.get(url).getResponse());
        String responseCircuit = new JsonParser().parse(response).getAsJsonObject().get("circuit").toString();

        return Circuit.fromJsonObject(responseCircuit, mGson);
    }


    @Override public boolean updateAccountInformations() throws IParapheurException {

        String params = "user=" + account.getLogin();
        String url = buildUrl(RESOURCE_USER_INFO, params, true);
        RequestResponse response = RestUtils.get(url);

        if (response.getResponse() != null) {

            JsonExplorer json = new JsonExplorer(response.getResponse());
            String firstName = json.findObject("data").findObject("properties").optString(USER_INFO_FIRST_NAME);
            String lastName = json.findObject("data").findObject("properties").optString(USER_INFO_LAST_NAME);

            if (StringsUtils.areNotEmpty(firstName, lastName)) account.setUserFullName(firstName + " " + lastName);
        }

        return true;
    }


    // <editor-fold desc="Annotations">


    protected @NonNull String getAnnotationsUrlSuffix(@NonNull String dossierId, @NonNull String documentId) {
        return String.format(RESOURCE_ANNOTATIONS, dossierId, documentId);
    }


    protected @NonNull String getAnnotationUrlSuffix(@NonNull String dossierId, @NonNull String documentId, @NonNull String annotationId) {
        return String.format(RESOURCE_ANNOTATION, dossierId, documentId, annotationId);
    }


    @Override public SerializableSparseArray<PageAnnotations> getAnnotations(@NonNull String dossierId, @NonNull String documentId)
            throws IParapheurException {
        String url = buildUrl(getAnnotationsUrlSuffix(dossierId, documentId));
        return modelMapper.getAnnotations(RestUtils.get(url));
    }


    @Override public String createAnnotation(@NonNull String dossierId, @NonNull String documentId, @NonNull Annotation annotation, int page)
            throws IParapheurException {

        // Build json object

        JSONStringer annotationJson = new JSONStringer();

        try {
            annotationJson.object();
            {
                annotationJson.key("rect").object();
                {
                    annotationJson.key("topLeft");
                    annotationJson.object();
                    annotationJson.key("x").value(annotation.getRect().left);
                    annotationJson.key("y").value(annotation.getRect().top);
                    annotationJson.endObject();

                    annotationJson.key("bottomRight");
                    annotationJson.object();
                    annotationJson.key("x").value(annotation.getRect().right);
                    annotationJson.key("y").value(annotation.getRect().bottom);
                    annotationJson.endObject();
                }
                annotationJson.endObject();

                annotationJson.key("author").value(annotation.getAuthor());
                annotationJson.key("date").value(annotation.getDate());
                annotationJson.key("page").value(page);
                annotationJson.key("text").value(annotation.getText() != null ? annotation.getText() : "");
                annotationJson.key("type").value("rect");

            }
            annotationJson.endObject();
        } catch (JSONException e) {
            throw new RuntimeException("Une erreur est survenue lors de la création de l'annotation", e);
        }

        // Send request

        String url = buildUrl(getAnnotationsUrlSuffix(dossierId, documentId));
        RequestResponse response = RestUtils.post(url, annotationJson.toString());

        if (response != null && response.getCode() == HttpURLConnection.HTTP_OK) {
            JSONObject idObj = response.getResponse();

            if (idObj != null) return idObj.optString("id", null);
        }

        return null;
    }


    @Override
    public void updateAnnotation(@NonNull String dossierId, @NonNull String documentId, @NonNull Annotation annotation, int page) throws IParapheurException {

        // Build Json object

        JSONStringer annotationJson = new JSONStringer();

        try {
            annotationJson.object();
            {
                annotationJson.key("rect").object();
                {
                    annotationJson.key("topLeft");
                    annotationJson.object();
                    annotationJson.key("x").value(annotation.getRect().left);
                    annotationJson.key("y").value(annotation.getRect().top);
                    annotationJson.endObject();

                    annotationJson.key("bottomRight");
                    annotationJson.object();
                    annotationJson.key("x").value(annotation.getRect().right);
                    annotationJson.key("y").value(annotation.getRect().bottom);
                    annotationJson.endObject();
                }
                annotationJson.endObject();

                annotationJson.key("author").value(annotation.getAuthor());
                annotationJson.key("date").value(annotation.getDate());
                annotationJson.key("page").value(page);
                annotationJson.key("text").value(annotation.getText() != null ? annotation.getText() : "");
                annotationJson.key("type").value("rect");
                annotationJson.key("id").value(annotation.getUuid());
                annotationJson.key("uuid").value(annotation.getUuid());
            }
            annotationJson.endObject();
        } catch (JSONException e) {
            throw new RuntimeException("Une erreur est survenue lors de l'enregistrement de l'annotation", e);
        }

        // Send request

        String url = buildUrl(getAnnotationUrlSuffix(dossierId, documentId, annotation.getUuid()));
        RequestResponse response = RestUtils.put(url, annotationJson.toString(), true);

        if (response == null || response.getCode() != HttpURLConnection.HTTP_OK) throw new IParapheurException(R.string.Error_on_annotation_update, "");
    }


    @Override
    public void deleteAnnotation(@NonNull String dossierId, @NonNull String documentId, @NonNull String annotationId, int page) throws IParapheurException {

        String url = buildUrl(getAnnotationUrlSuffix(dossierId, documentId, annotationId));
        RestUtils.delete(url, true);
    }

    // </editor-fold desc="Annotations">


    // <editor-fold desc="Actions">


    @Override
    public boolean viser(Dossier dossier, String annotPub, String annotPriv, String bureauId) throws IParapheurException {
        String actionUrl = String.format(ACTION_VISA, dossier.getId());
        try {
            JSONObject json = new JSONObject();
            json.put(PARAM_CURRENT_DESK, bureauId);
            json.put(PARAM_PUBLIC_ANNOTATION, annotPub);
            json.put(PARAM_PRIVATE_ANNOTATION, annotPriv);
            RequestResponse response = RestUtils.post(buildUrl(actionUrl), json.toString());
            return (response != null && response.getCode() == HttpURLConnection.HTTP_OK);
        } catch (JSONException e) {
            throw new RuntimeException("Une erreur est survenue lors du visa", e);
        }
    }


    @Override
    public boolean seal(Dossier dossier, String annotPub, String annotPriv, String bureauId) throws IParapheurException {

        String actionUrl = String.format(ACTION_SEAL, dossier.getId());
        try {
            JSONObject json = new JSONObject();
            json.put(PARAM_CURRENT_DESK, bureauId);
            json.put(PARAM_PUBLIC_ANNOTATION, annotPub);
            json.put(PARAM_PRIVATE_ANNOTATION, annotPriv);
            RequestResponse response = RestUtils.post(buildUrl(actionUrl), json.toString());
            return (response != null && response.getCode() == HttpURLConnection.HTTP_OK);
        } catch (JSONException e) {
            throw new RuntimeException("Une erreur est survenue lors du cachet", e);
        }
    }


    @Override
    public boolean signPapier(String dossierId, String bureauId) throws IParapheurException {
        String actionUrl = String.format(ACTION_SIGNATURE_PAPIER, dossierId);

        JSONStringer jsonStringer = new JSONStringer();
        try {
            jsonStringer.object();
            jsonStringer.key(PARAM_CURRENT_DESK).value(bureauId);
            jsonStringer.endObject();
        } catch (JSONException e) {
            throw new RuntimeException("Une erreur est survenue lors de la conversion en signature papier", e);
        }

        RequestResponse response = RestUtils.post(buildUrl(actionUrl), jsonStringer.toString());
        return (response != null && response.getCode() == HttpURLConnection.HTTP_OK);
    }


    @Override
    public boolean archiver(String dossierId, String archiveTitle, boolean withAnnexes, String bureauId) throws IParapheurException {
        /** FIXME : weird copy/paste. Maybe it has no utility too.
         String actionUrl = String.format(ACTION_SIGNATURE, dossierId);
         try {
         JSONObject json = new JSONObject();
         json.put("bureauCourant", bureauId);
         json.put("name", archiveTitle);
         json.put("annexesInclude", withAnnexes);
         RequestResponse response = RESTUtils.post(buildUrl(actionUrl), json.toString());
         return (response != null && response.getCode() == HttpURLConnection.HTTP_OK);

         } catch (JSONException e) {
         throw new RuntimeException("Une erreur est survenue lors de l'archivage", e);
         }*/
        return false;
    }


    @Override
    public boolean envoiTdtHelios(String dossierId, String annotPub, String annotPriv, String bureauId) throws IParapheurException {
        String actionUrl = String.format(ACTION_TDT_HELIOS, dossierId);
        try {
            JSONObject json = new JSONObject();
            json.put(PARAM_CURRENT_DESK, bureauId);
            json.put(PARAM_PUBLIC_ANNOTATION, annotPub);
            json.put(PARAM_PRIVATE_ANNOTATION, annotPriv);
            RequestResponse response = RestUtils.post(buildUrl(actionUrl), json.toString());
            return (response != null && response.getCode() == HttpURLConnection.HTTP_OK);

        } catch (JSONException e) {
            throw new RuntimeException("Une erreur est survenue lors de l'envoi au TdT (Helios)", e);
        }
    }


    @Override
    public boolean envoiTdtActes(String dossierId, String nature, String classification, String numero, long dateActes, String objet, String annotPub, String annotPriv, String bureauId) throws IParapheurException {
        String actionUrl = String.format(ACTION_TDT_ACTES, dossierId);
        try {
            JSONObject json = new JSONObject();
            json.put(PARAM_CURRENT_DESK, bureauId);
            json.put(PARAM_PUBLIC_ANNOTATION, annotPub);
            json.put(PARAM_PRIVATE_ANNOTATION, annotPriv);
            json.put("objet", objet);
            json.put("nature", nature);
            json.put("classification", classification);
            json.put("numero", numero);
            json.put("dateActes", dateActes);
            RequestResponse response = RestUtils.post(buildUrl(actionUrl), json.toString());
            return (response != null && response.getCode() == HttpURLConnection.HTTP_OK);

        } catch (JSONException e) {
            throw new RuntimeException("Une erreur est survenue lors de l'envoi au TdT (ACTES)", e);
        }
    }


    @Override
    public boolean envoiMailSec(String dossierId, List<String> destinataires, List<String> destinatairesCC, List<String> destinatairesCCI, String sujet, String message, String password, boolean showPassword, boolean annexesIncluded, String bureauId) throws IParapheurException {

        String actionUrl = String.format(ACTION_MAILSEC, dossierId);
        try {
            JSONObject json = new JSONObject();
            json.put(PARAM_CURRENT_DESK, bureauId);
            json.put("destinataires", destinataires);
            json.put("destinatairesCC", destinatairesCC);
            json.put("destinatairesCCI", destinatairesCCI);
            json.put("objet", sujet);
            json.put("message", message);
            json.put("password", password);
            json.put("showpass", showPassword);
            json.put("annexesIncluded", annexesIncluded);
            RequestResponse response = RestUtils.post(buildUrl(actionUrl), json.toString());
            return (response != null && response.getCode() == HttpURLConnection.HTTP_OK);

        } catch (JSONException e) {
            throw new RuntimeException("Une erreur est survenue lors de l'envoi par mail sécurisé", e);
        }
    }


    @Override
    public boolean rejeter(String dossierId, String annotPub, String annotPriv, String bureauId) throws IParapheurException {
        String actionUrl = String.format(ACTION_REJET, dossierId);
        try {
            JSONObject json = new JSONObject();
            json.put(PARAM_CURRENT_DESK, bureauId);
            json.put(PARAM_PUBLIC_ANNOTATION, annotPub);
            json.put(PARAM_PRIVATE_ANNOTATION, annotPriv);
            RequestResponse response = RestUtils.post(buildUrl(actionUrl), json.toString());
            return (response != null && response.getCode() == HttpURLConnection.HTTP_OK);

        } catch (JSONException e) {
            throw new RuntimeException("Une erreur est survenue lors du rejet", e);
        }
    }


    // </editor-fold desc="Actions">


    private DataToSign getDataToSign47(@NonNull String deskId, @NonNull String folderId, @NonNull String publicCertBase64) throws IParapheurException {
        Log.d(LOG_TAG, "getDataToSign47 4.7");

        Uri uri = new Uri.Builder()
                .scheme(HTTPS).authority(getAuthority())
                .path(PATH_PARAPHEUR).appendPath("signature").appendPath(deskId).appendPath(folderId)
                .build();

        // Instantiate the RequestQueue.

        RequestQueue queue = Volley.newRequestQueue(context);
        RequestFuture<List<DataToSign>> future = RequestFuture.newFuture();
        GetSignInfoRequest body = new GetSignInfoRequest(publicCertBase64);

        Log.v(LOG_TAG, uri.toString());

        try {
            // Request from the provided URL
            JacksonRequest<List<DataToSign>> jacksonRequest = new JacksonRequest<>(
                    POST, uri.toString(), body,
                    new TypeReference<List<DataToSign>>() {},
                    future, future, account, new ObjectMapper()
            );

            // Add the request to the RequestQueue
            queue.add(jacksonRequest);

            return future.get().get(0);

        } catch (InterruptedException | ExecutionException | JsonProcessingException e) {
            e.printStackTrace();
            throw new IParapheurException(e);
        }
    }


    private SignInfo getSignInfo46(@NonNull String deskId, @NonNull String folderId) throws IParapheurException {
        Log.d(LOG_TAG, "getSignInfo46 4.7");

        Uri uri = new Uri.Builder()
                .scheme(HTTPS).authority(getAuthority())
                .path(PATH_PARAPHEUR).appendPath("dossiers").appendPath(folderId).appendPath("getSignInfo")
                .appendQueryParameter(PARAM_ALF_TICKET, account.getTicket())
                .appendQueryParameter(PARAM_CURRENT_DESK, deskId)
                .build();

        // Instantiate the RequestQueue.

        RequestQueue queue = Volley.newRequestQueue(context);
        RequestFuture<SignInfo> future = RequestFuture.newFuture();

        Log.v(LOG_TAG, uri.toString());

        try {
            // Request from the provided URL.
            JacksonRequest<SignInfo> jacksonRequest = new JacksonRequest<>(
                    GET, uri.toString(),
                    null, new TypeReference<SignInfo>() {},
                    future, future, account,
                    new ObjectMapper().configure(UNWRAP_ROOT_VALUE, true)
            );

            // Add the request to the RequestQueue.
            queue.add(jacksonRequest);

            SignInfo signInfo = future.get();
            signInfo.setLegacy(true);
            return signInfo;

        } catch (InterruptedException | ExecutionException | JsonProcessingException e) {
            throw new IParapheurException(e);
        }
    }


    @Override public DataToSign getDataToSign(@NonNull String deskId, @NonNull String folderId, @NonNull String publicCertBase64)
            throws IParapheurException, JsonProcessingException {

        try { // IP 4.7+ case

            DataToSign dataToSign = getDataToSign47(deskId, folderId, publicCertBase64);

            for (int i = 0; i < dataToSign.getDataToSignBase64List().size(); i++) {
                RemoteDocument remoteDocument = new RemoteDocument(
                        "doc_" + i, null, dataToSign.getDataToSignBase64List().get(i), null
                );
                dataToSign.getRemoteDocuments().add(remoteDocument);
            }

            return dataToSign;

        } catch (IParapheurException e) { // IP 4.6 case

            // Retrieving signInfo and parsing it...

            Log.d(LOG_TAG, "Fallback to 4.6 methods...");
            SignInfo signInfo = getSignInfo46(deskId, folderId);
            List<RemoteDocument> remoteDocumentList = new ArrayList<>();

            String digestAlgo = DIGEST_ALGORITHM_SHA256;
            for (int i = 0; i < signInfo.getHashesToSignBase64().size(); i++) {

                String hash = signInfo.getHashesToSignBase64().get(i);
                String remoteDocumentId = ((signInfo.getPesIds() != null) && (i < signInfo.getPesIds().size()) && (signInfo.getPesIds().get(i) != null))
                                          ? signInfo.getPesIds().get(i)
                                          : String.format("doc_%d", i);

                remoteDocumentList.add(new RemoteDocument(remoteDocumentId, hash, null, null));
                if (hash.length() == 28) {
                    digestAlgo = DIGEST_ALGORITHM_SHA1;
                }
            }

            Uri uri = new Uri.Builder()
                    .scheme(HTTPS).authority(getAuthority())
                    .path(PATH_CRYPTO).appendPath("api").appendPath("generateDataToSign")
                    .build();

            ObjectMapper objectMapper = new ObjectMapper();
            DataToSignRequest request = new DataToSignRequest();
            request.setPublicKeyBase64(publicCertBase64);
            request.setSignatureFormat(signInfo.getFormat());
            request.setRemoteDocumentList(remoteDocumentList);

            RequestResponse response = RestUtils.post(uri.toString(), objectMapper.writeValueAsString(request));
            DataToSign result = objectMapper.readValue(response.getResponse().toString(), new TypeReference<DataToSignLegacy>() {});

            // Building proper response

            for (int i = 0; i < result.getDataToSignBase64List().size(); i++) {
                String dataToSign = result.getDataToSignBase64List().get(i);
                remoteDocumentList.get(i).setDataToSignBase64(dataToSign);
            }

            result.setRemoteDocuments(remoteDocumentList);
            result.setDigestAlgorithm(digestAlgo);
            result.setFormat(signInfo.getFormat());
            result.setLegacy(true);

            Log.d(LOG_TAG, "DataToSign : " + result);
            return result;
        }
    }


    private boolean sendFinalSignature(@NonNull String deskId, @NonNull String folderId, @Nullable String publicAnnotation,
                                       @Nullable String privateAnnotation, String wrappedSignValue) throws IParapheurException {

        String actionUrl = String.format(ACTION_SIGNATURE, folderId);

        JSONStringer jsonStringer = new JSONStringer();
        try {
            jsonStringer.object();
            {
                jsonStringer.key(PARAM_CURRENT_DESK).value(deskId);
                jsonStringer.key(PARAM_PUBLIC_ANNOTATION).value(publicAnnotation);
                jsonStringer.key(PARAM_PRIVATE_ANNOTATION).value(privateAnnotation);
                jsonStringer.key("signature").value(wrappedSignValue);
            }
            jsonStringer.endObject();
        } catch (JSONException e) {
            throw new RuntimeException("Une erreur est survenue lors de la signature", e);
        }

        RequestResponse response = RestUtils.post(buildUrl(actionUrl), jsonStringer.toString());
        return (response != null && response.getCode() == HttpURLConnection.HTTP_OK);
    }


    private void sign47(@NonNull String deskId, @NonNull String folderId, @NonNull List<RemoteDocument> remoteDocumentList,
                        long signatureDateTime, @NonNull String publicKeyBase64, @Nullable String publicAnnotation, @Nullable String privateAnnotation)
            throws IParapheurException {

        Signature47Request.SignatureElement signatureElements = new Signature47Request.SignatureElement();
        for (RemoteDocument remoteDocument : remoteDocumentList) {
            signatureElements.getSignature().add(remoteDocument.getSignatureBase64());
            signatureElements.getSignatureDateTime().add(signatureDateTime);
        }

        Signature47Request body = new Signature47Request();
        body.setBureauCourant(deskId);
        body.setCertificate(publicKeyBase64);
        body.setAnnotPriv(privateAnnotation);
        body.setAnnotPub(publicAnnotation);
        body.setSignature(signatureElements);

        Log.d(LOG_TAG, "sign47 documentList:" + remoteDocumentList);
        Log.d(LOG_TAG, "sign47 body:" + body);

        // Send request

        Uri uri = new Uri.Builder()
                .scheme(HTTPS).authority(getAuthority())
                .path(PATH_PARAPHEUR).appendPath("dossiers").appendPath(folderId).appendPath("signature")
                .build();

        RequestQueue queue = Volley.newRequestQueue(context);
        RequestFuture<Void> future = RequestFuture.newFuture();

        Log.v(LOG_TAG, uri.toString());

        try {
            // Request from the provided URL
            JacksonRequest<Void> jacksonRequest = new JacksonRequest<>(
                    POST, uri.toString(), body,
                    new TypeReference<Void>() {},
                    future, future, account, new ObjectMapper()
            );

            // Add the request to the RequestQueue
            queue.add(jacksonRequest);
            future.get();

        } catch (InterruptedException | ExecutionException | JsonProcessingException e) {
            e.printStackTrace();
            throw new IParapheurException(e);
        }
    }


    private void sign46(@NonNull String deskId, @NonNull String folderId, @NonNull List<RemoteDocument> remoteDocumentList,
                        long signatureDateTime, @NonNull String publicKeyBase64,
                        @NonNull String signatureFormat, @Nullable String publicAnnotation, @Nullable String privateAnnotation)
            throws JsonProcessingException, IParapheurException {

        // Build the final signature with the Crypto service

        Uri uri = new Uri.Builder()
                .scheme(HTTPS).authority(getAuthority())
                .path(PATH_CRYPTO).appendPath("api").appendPath("generateSignature")
                .build();

        ObjectMapper objectMapper = new ObjectMapper();
        Signature46Request request = new Signature46Request();
        request.setPublicKeyBase64(publicKeyBase64);
        request.setSignatureFormat(signatureFormat);
        request.setRemoteDocumentList(remoteDocumentList);
        request.setSignatureDateTime(signatureDateTime);
        request.setPublicAnnotation(publicAnnotation);
        request.setPrivateAnnotation(privateAnnotation);

        RequestResponse response = RestUtils.post(uri.toString(), objectMapper.writeValueAsString(request));
        SignatureResult signatureResult = objectMapper.readValue(response.getResponse().toString(), new TypeReference<SignatureResult>() {});

        // Send back the final signature

        String wrappedSignature = TextUtils.join(",", signatureResult.getSignatureResultBase64List());
        sendFinalSignature(deskId, folderId, publicAnnotation, privateAnnotation, wrappedSignature);

    }


    @Override public void sign(@NonNull String deskId, @NonNull String folderId, @NonNull List<RemoteDocument> remoteDocumentList,
                               long signatureDateTime, @NonNull String publicKeyBase64, @NonNull String signatureFormat,
                               @Nullable String publicAnnotation, @Nullable String privateAnnotation, boolean isLegacy)
            throws IParapheurException, JsonProcessingException {

        if (isLegacy) {
            sign46(deskId, folderId, remoteDocumentList, signatureDateTime, publicKeyBase64, signatureFormat,
                    publicAnnotation, privateAnnotation);
        }
        else {
            sign47(deskId, folderId, remoteDocumentList, signatureDateTime, publicKeyBase64, publicAnnotation, privateAnnotation);
        }
    }


}
