/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.service.rest.api;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.List;

import fr.libriciel.iparapheur.model.Annotation;
import fr.libriciel.iparapheur.model.Bureau;
import fr.libriciel.iparapheur.model.Circuit;
import fr.libriciel.iparapheur.model.DataToSign;
import fr.libriciel.iparapheur.model.Dossier;
import fr.libriciel.iparapheur.model.Filter;
import fr.libriciel.iparapheur.model.PageAnnotations;
import fr.libriciel.iparapheur.model.ParapheurType;
import fr.libriciel.iparapheur.service.rest.request.RemoteDocument;
import fr.libriciel.iparapheur.utils.IParapheurException;
import fr.libriciel.iparapheur.utils.SerializableSparseArray;


public interface RestClientInterface {

    String BASE_PATH = "https://m-";


    int test() throws IParapheurException;


    List<Bureau> getDesks() throws IParapheurException;


    Dossier getFolder(String bureauId, String dossierId) throws IParapheurException;


    List<Dossier> getFolders(@NonNull String bureauId, @Nullable Filter filter) throws IParapheurException;


    List<ParapheurType> getTypology() throws IParapheurException;


    Circuit getWorkflow(String dossierId) throws IParapheurException;


    /**
     * @return les annotations graphiques déposées sur le document principal du dossier
     * @throws IParapheurException
     */
    SerializableSparseArray<PageAnnotations> getAnnotations(@NonNull String dossierId, @NonNull String documentId) throws IParapheurException;


    /**
     * @return l'id de l'annotation crééeles annotations graphiques déposées sur le document principal du dossier
     * @throws IParapheurException
     */
    String createAnnotation(@NonNull String dossierId, @NonNull String documentId, @NonNull Annotation annotation, int page) throws IParapheurException;


    void updateAnnotation(@NonNull String dossierId, @NonNull String documentId, @NonNull Annotation annotation, int page) throws IParapheurException;


    void deleteAnnotation(@NonNull String dossierId, @NonNull String documentId, @NonNull String annotationId, int page) throws IParapheurException;


    boolean updateAccountInformations() throws IParapheurException;


    boolean downloadFile(@NonNull String url, @NonNull String path) throws IParapheurException;


    boolean downloadCertificate(@NonNull String urlString, @NonNull String certificateLocalPath) throws IParapheurException;


    boolean viser(Dossier dossier, String annotPub, String annotPriv, String bureauId) throws IParapheurException;


    boolean seal(Dossier dossier, String annotPub, String annotPriv, String bureauId) throws IParapheurException;


    boolean signPapier(String dossierId, String bureauId) throws IParapheurException;


    boolean archiver(String dossierId, String archiveTitle, boolean withAnnexes, String bureauId) throws IParapheurException;


    boolean envoiTdtHelios(String dossierId, String annotPub, String annotPriv, String bureauId) throws IParapheurException;


    boolean envoiTdtActes(String dossierId, String nature, String classification, String numero, long dateActes, String objet, String annotPub, String annotPriv, String bureauId) throws IParapheurException;


    boolean envoiMailSec(String dossierId, List<String> destinataires, List<String> destinatairesCC, List<String> destinatairesCCI, String sujet, String message, String password, boolean showPassword, boolean annexesIncluded, String bureauId) throws IParapheurException;


    boolean rejeter(String dossierId, String annotPub, String annotPriv, String bureauId) throws IParapheurException;


    DataToSign getDataToSign(@NonNull String deskId, @NonNull String folderId, @NonNull String publicKeyBase64)
            throws IParapheurException, JsonProcessingException;


    void sign(@NonNull String deskId, @NonNull String folderId, @NonNull List<RemoteDocument> remoteDocumentList, long signatureDateTime,
              @NonNull String publicKeyBase64, @NonNull String signatureFormat, @Nullable String publicAnnotation, @Nullable String privateAnnotation,
              boolean isLegacy) throws IParapheurException, JsonProcessingException;


}