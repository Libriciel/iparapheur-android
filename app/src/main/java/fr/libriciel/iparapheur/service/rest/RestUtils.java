/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.service.rest;

import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONStringer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;

import fr.libriciel.iparapheur.R;
import fr.libriciel.iparapheur.model.Account;
import fr.libriciel.iparapheur.model.RequestResponse;
import fr.libriciel.iparapheur.utils.IParapheurException;
import io.sentry.Sentry;

import static fr.libriciel.iparapheur.service.rest.api.RestClientApi4.SESSION_TIMEOUT;
import static fr.libriciel.iparapheur.utils.StringsUtils.longLogV;


public class RestUtils {

    private static final String LOG_TAG = "RestUtils";


    public static RequestResponse post(String url, String body) throws IParapheurException {
        longLogV(LOG_TAG, "POST request on : " + url);
        longLogV(LOG_TAG, "with body : " + body);
        RequestResponse res;
        OutputStream output;

        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();

            connection.setDoOutput(true); // Triggers POST.
            connection.setChunkedStreamingMode(0);
            connection.setConnectTimeout(10000);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept-Charset", "UTF-8");
            output = connection.getOutputStream();
            output.write(body.getBytes());
            res = new RequestResponse(connection);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error on URL", e);
            throw new IParapheurException(R.string.http_error_malformed_url, url);
        } catch (ProtocolException e) {
            Log.e(LOG_TAG, "Error on protocol", e);
            throw new IParapheurException(R.string.http_error_405, null);
        } catch (UnknownHostException e) {
            Log.e(LOG_TAG, "Error fetching host", e);
            throw new IParapheurException(R.string.error_no_internet, null);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error on request", e);
            throw new IParapheurException(R.string.http_error_400, null);
        }

        return res;
    }


    public static RequestResponse put(String url, String body) throws IParapheurException {
        return put(url, body, false);
    }


    public static RequestResponse put(String url, String body, boolean ignoreResponseData) throws IParapheurException {
        longLogV(LOG_TAG, "POST request on : " + url);
        longLogV(LOG_TAG, "with body : " + body);
        RequestResponse res;
        OutputStream output;

        try {
            HttpURLConnection connection = (HttpsURLConnection) new URL(url).openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("PUT");
            connection.setChunkedStreamingMode(0);
            //connection.setReadTimeout(10000);
            connection.setConnectTimeout(10000);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept-Charset", "UTF-8");
            output = connection.getOutputStream();
            output.write(body.getBytes());
            res = new RequestResponse(connection, ignoreResponseData);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error parsing URL", e);
            throw new IParapheurException(R.string.http_error_malformed_url, url);
        } catch (ProtocolException e) {
            throw new IParapheurException(R.string.http_error_405, null);
        } catch (IOException e) {
            throw new IParapheurException(R.string.http_error_400, null);
        }

        return res;
    }


    public static RequestResponse get(@NonNull String url) throws IParapheurException {
        longLogV(LOG_TAG, "GET request on : " + url);
        RequestResponse res;

        try {

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();

            connection.setRequestMethod("GET");
            connection.setDoOutput(false);
            connection.setChunkedStreamingMode(0);
            connection.setConnectTimeout(10000);
            connection.setRequestProperty("Accept-Charset", "UTF-8");

            res = new RequestResponse(connection);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error parsing URL", e);
            throw new IParapheurException(R.string.http_error_malformed_url, url);
        } catch (ProtocolException e) {
            Log.e(LOG_TAG, "Error on protocol", e);
            throw new IParapheurException(R.string.http_error_405, null);
        } catch (IOException e) {
            Log.e(LOG_TAG, "IO Error", e);
            throw new IParapheurException(R.string.http_error_400, null);
        }

        return res;
    }


    public static RequestResponse delete(String url) throws IParapheurException {
        return delete(url, false);
    }


    public static RequestResponse delete(String url, boolean ignoreResponseData) throws IParapheurException {
        longLogV(LOG_TAG, "GET request on : " + url);
        RequestResponse res;

        try {
            HttpURLConnection connection = (HttpsURLConnection) new URL(url).openConnection();

            connection.setRequestMethod("DELETE");
            connection.setDoOutput(false);
            connection.setChunkedStreamingMode(0);
            //connection.setReadTimeout(10000);
            connection.setConnectTimeout(10000);
            //connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept-Charset", "UTF-8");

            res = new RequestResponse(connection, ignoreResponseData);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error on URL", e);
            throw new IParapheurException(R.string.http_error_malformed_url, url);
        } catch (ProtocolException e) {
            throw new IParapheurException(R.string.http_error_405, null);
        } catch (IOException e) {
            throw new IParapheurException(R.string.http_error_400, null);
        }

        return res;
    }


    public static InputStream downloadFile(String url) throws IParapheurException {
        longLogV(LOG_TAG, "GET (download file) request on : " + url);
        InputStream fileStream;
        HttpURLConnection connection;
        try {
            connection = (HttpsURLConnection) new URL(url).openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(false);
            connection.setChunkedStreamingMode(0);
            fileStream = connection.getInputStream();
        } catch (IOException e) {
            throw new IParapheurException(R.string.http_error_400, null);
        }
        return fileStream;
    }


    public static @Nullable String getAuthenticationJsonData(@NonNull Account account) {

        String requestContent = null;

        try {
            JSONStringer requestStringer = new JSONStringer();
            requestStringer.object();
            requestStringer.key("username").value(account.getLogin());
            requestStringer.key("password").value(account.getPassword());
            requestStringer.endObject();

            requestContent = requestStringer.toString();
        } catch (JSONException e) {
            Sentry.capture(e);
            Log.e(LOG_TAG, "Json error", e);
        }

        return requestContent;
    }


    public static boolean hasValidTicket(@NonNull Account account) {

        String ticket = account.getTicket();
        Long time = new Date().getTime();

        return ((!TextUtils.isEmpty(ticket)) && (account.getLastRequest() != null)
                && ((time - account.getLastRequest().getTime()) < SESSION_TIMEOUT));
    }


}
