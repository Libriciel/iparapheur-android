/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.service.rest.api;

import android.content.Context;

import androidx.annotation.NonNull;

import fr.libriciel.iparapheur.R;
import fr.libriciel.iparapheur.model.Account;
import fr.libriciel.iparapheur.model.RequestResponse;
import fr.libriciel.iparapheur.service.rest.RestUtils;
import fr.libriciel.iparapheur.utils.IParapheurException;
import fr.libriciel.iparapheur.utils.JsonExplorer;


public class RestClientFactory {

    public static final int API_VERSION_MAX = 4;
    private static final String RESOURCE_API_VERSION = "/parapheur/api/getApiLevel";


    public int getApiVersion(@NonNull Account account, @NonNull Context context) throws IParapheurException {
        return getApiVersion(account, context, true, false);
    }


    /**
     * Renvoie la version d'API du serveur i-Parapheur associé à ce compte.
     * Cette méthode peut faire une requête au serveur, il faut donc l'appeler dans
     * un thread (ex. AsyncTask).
     *
     * @param account le compte pour lequel on veur récupérer la version de l'API
     * @return in entier représentant la version de l'API.
     */
    private int getApiVersion(@NonNull Account account, @NonNull Context context, boolean withTenant, boolean withAuthentication) throws IParapheurException {

        // Default check

        Integer apiVersion = account.getApiVersion();
        if ((apiVersion != null))
            return apiVersion;

        // Request

        String url = new RestClientApi4(account, context)
                .buildUrl(RESOURCE_API_VERSION, null, withAuthentication, withTenant);

        try {
            RequestResponse response = RestUtils.get(url);
            apiVersion = new JsonExplorer(response.getResponse()).optInt("level", -1);
        } catch (IParapheurException e) {

            // 404 errors may be Tenant unavailability
            // So we check for non-tenant reachability with a recursive call
            if ((e.getResId() == R.string.http_error_404) && withTenant) {

                boolean isReachableWithoutTenant = true;
                try { getApiVersion(account, context, false, withAuthentication); } catch (IParapheurException subEx) {
                    isReachableWithoutTenant = (subEx.getResId() != R.string.http_error_404);
                }

                if (isReachableWithoutTenant) throw new IParapheurException(R.string.test_tenant_not_exist, null);
                else throw new IParapheurException(R.string.test_unreachable, null);
            }

            // Certificate errors may be Tenant wrong parameter
            // So we check for non-tenant reachability with a recursive call
            if ((e.getResId() == R.string.error_server_not_configured) && withTenant) {

                boolean isReachableWithoutTenant = true;
                try { getApiVersion(account, context, false, withAuthentication); } catch (IParapheurException subEx) {
                    isReachableWithoutTenant = (subEx.getResId() != R.string.error_server_not_configured);
                }

                if (isReachableWithoutTenant) throw new IParapheurException(R.string.error_server_not_configured_for_tenant, null);

                throw e;
            }
            else {
                throw e;
            }
        }

        if (apiVersion == -1) {
            throw new IParapheurException(R.string.error_mismatch_versions, account.getTitle());
        }

        account.setApiVersion(apiVersion);
        return apiVersion;
    }


    public RestClientInterface createRestClient(@NonNull Account account, @NonNull Context context) throws IParapheurException {

        RestClientInterface apiClient;
        Integer apiVersion = account.getApiVersion();

        if ((account.getApiVersion() == null) || (account.getApiVersion() < 0)) {
            apiVersion = this.getApiVersion(account, context);
        }

        if (apiVersion == 4) {
            apiClient = new RestClientApi4(account, context);
        }
        else {
            throw new IParapheurException(R.string.Error_incompatible_parapheur_version, account.getTitle());
        }

        return apiClient;
    }


}
