/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.service.rest.mapper;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.Map;

import fr.libriciel.iparapheur.model.Annotation;
import fr.libriciel.iparapheur.model.PageAnnotations;
import fr.libriciel.iparapheur.model.RequestResponse;
import fr.libriciel.iparapheur.utils.JsonExplorer;
import fr.libriciel.iparapheur.utils.SerializableSparseArray;


public class ModelMapper {

    protected static String DOSSIER_CIRCUIT = "circuit";

    protected static String CIRCUIT_ETAPES_DATE_VALIDATION = "dateValidation";
    protected static String CIRCUIT_ETAPES_APPROVED = "approved";
    protected static String CIRCUIT_ETAPES_REJECTED = "rejected";
    protected static String CIRCUIT_ETAPES_PARAPHEUR_NAME = "parapheurName";
    protected static String CIRCUIT_ETAPES_SIGNATAIRE = "signataire";
    protected static String CIRCUIT_ETAPES_ACTION_DEMANDEE = "actionDemandee";
    protected static String CIRCUIT_ETAPES_PUBLIC_ANNOTATIONS = "annotPub";


    public SerializableSparseArray<PageAnnotations> getAnnotations(RequestResponse response) {
        SerializableSparseArray<PageAnnotations> annotations = new SerializableSparseArray<>();

        // Default case

        if (response.getResponseArray() == null) return annotations;

        // Parsing

        JsonExplorer jsonExplorer = new JsonExplorer(response.getResponseArray());

        for (int etapeNumber = 0; etapeNumber < jsonExplorer.getCurrentArraySize(); etapeNumber++) {
            JsonObject pagesDict = jsonExplorer.find(etapeNumber).optCurrentJsonObject(new JsonObject());

            for (Map.Entry<String, JsonElement> pageDict : pagesDict.entrySet()) {
                JsonExplorer jsonPageExplorer = new JsonExplorer(pageDict.getValue());

                PageAnnotations pageAnnotations = new PageAnnotations();

                for (int annotationNumber = 0; annotationNumber < jsonPageExplorer.getCurrentArraySize(); annotationNumber++) {
                    pageAnnotations.add(new Annotation(jsonPageExplorer.find(annotationNumber).optCurrentJsonObject(new JsonObject()), Integer.parseInt(pageDict.getKey()), etapeNumber));
                }

                annotations.put(Integer.parseInt(pageDict.getKey()), pageAnnotations);
            }
        }

        return annotations;
    }

}
