/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.utils;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Enumeration;

import lombok.Getter;

import static android.util.Base64.NO_WRAP;


public final class RsaSigner {

    private static final String LOG_TAG = "RsaSigner";
    private static final String SHA256_WITH_RSA = "SHA256WithRSA";
    private static final String SHA1_WITH_RSA = "SHA1WithRSA";
    public static final String SHA256 = "SHA256";
    public static final String SHA1 = "SHA1";

    private KeyStore mKeystore;
    private @Getter String publicKeyBase64;
    private PrivateKey mPrivateKey;
    private final String mCertificatePath;
    private final String mKeystorePassword;
    private final String mAlias;
    private final String mAliasPassword;


    public RsaSigner(@NonNull String certificatePath, @NonNull String keystorePassword, @NonNull String alias, @NonNull String aliasPassword) {
        mCertificatePath = certificatePath;
        mKeystorePassword = keystorePassword;
        mAlias = alias;
        mAliasPassword = aliasPassword;
    }


    public @Nullable KeyStore loadKeyStore() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, UnrecoverableKeyException {

        mKeystore = KeyStore.getInstance("BKS");
        InputStream is = new FileInputStream(mCertificatePath);
        mKeystore.load(is, mKeystorePassword.toCharArray());
        Enumeration<String> aliasEnum = mKeystore.aliases();

        PrivateKey privateKey = null;
        X509Certificate certificate = null;

        while (aliasEnum.hasMoreElements()) {
            String keyName = aliasEnum.nextElement();
            privateKey = (PrivateKey) mKeystore.getKey(keyName, mKeystorePassword.toCharArray());
            certificate = (X509Certificate) mKeystore.getCertificate(keyName);
        }

        if (privateKey == null) {
            throw new CertificateException("Could not read private key from given file");
        }
        if (certificate == null) {
            throw new CertificateException("Could not read certificate from given file");
        }

        publicKeyBase64 = Base64.encodeToString(certificate.getEncoded(), NO_WRAP);
        return mKeystore;
    }


    public @Nullable PrivateKey loadPrivateKey() throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException {

        mPrivateKey = (PrivateKey) mKeystore.getKey(mAlias, mAliasPassword.toCharArray());
        return mPrivateKey;
    }


    public @Nullable Date getCertificateExpirationDate() {

        // Init checking

        if (mKeystore == null) throw new IllegalStateException("PKCS7Signer not properly initialized, call PKCS7Signer#loakKeystore() before");

        //

        Date result = null;

        try {
            Enumeration<String> aliases = mKeystore.aliases();
            while (aliases.hasMoreElements()) {
                String alias = aliases.nextElement();
                Date currentDate = ((X509Certificate) mKeystore.getCertificate(alias)).getNotAfter();
                if ((result == null) || ((currentDate != null) && result.after(currentDate))) result = currentDate;
            }
        } catch (KeyStoreException e) {
            Log.e(LOG_TAG, "Cert error", e);
            result = null;
        }

        return result;
    }


    public String sign(byte[] dataToSign, @NonNull String digestAlgorithm) throws NoSuchAlgorithmException, InvalidKeyException,
            SignatureException, IllegalStateException, IllegalArgumentException {

        String algorithm = TextUtils.equals(SHA1, digestAlgorithm) ? SHA1_WITH_RSA : SHA256_WITH_RSA;
        Log.i(LOG_TAG, "Sign with algorithm : " + algorithm);

        // Init checking

        if (mKeystore == null)
            throw new IllegalStateException("PKCS7Signer not properly initialized, call PKCS7Signer#loakKeystore() before");

        if (mPrivateKey == null)
            throw new IllegalStateException("PKCS7Signer not properly initialized, call PKCS7Signer#loakPrivateKey() before");

        if (dataToSign == null)
            throw new IllegalArgumentException("PKCS7Signer exception : Data to sign cannot be null");

        // Signed attributes computation

        Signature signature = Signature.getInstance(algorithm);
        signature.initSign(mPrivateKey);
        signature.update(dataToSign);
        return Base64.encodeToString(signature.sign(), NO_WRAP);
    }


}