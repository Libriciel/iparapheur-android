/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.utils;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import fr.libriciel.iparapheur.model.Action;
import fr.libriciel.iparapheur.model.Bureau;
import fr.libriciel.iparapheur.model.Document;
import fr.libriciel.iparapheur.model.Dossier;


public class DossierUtils {


    public static Comparator<Dossier> CREATION_DATE_COMPARATOR = (lhs, rhs) -> {

        if ((lhs == null) || (lhs.getDateCreation() == null)) {
            return Integer.MIN_VALUE;
        }

        if ((rhs == null) || (rhs.getDateCreation() == null)) {
            return Integer.MAX_VALUE;
        }

        return lhs.getDateCreation().compareTo(rhs.getDateCreation());
    };


    public static boolean haveActions(@NonNull Dossier dossier) {

        HashSet<Action> actionsAvailable = new HashSet<>();

        if (dossier.getActions() != null) actionsAvailable.addAll(dossier.getActions());

        actionsAvailable.remove(Action.EMAIL);
        actionsAvailable.remove(Action.JOURNAL);
        actionsAvailable.remove(Action.ENREGISTRER);

        return actionsAvailable.size() > 0;
    }


    public static boolean areDetailsAvailable(@NonNull Dossier dossier) {

        return (dossier.getCircuit() != null)
               && (dossier.getCircuit().getEtapeCircuitList() != null)
               && (!dossier.getCircuit().getEtapeCircuitList().isEmpty())
               && (!dossier.getDocumentList().isEmpty());
    }


    public static @Nullable Document findCurrentDocument(@Nullable Dossier dossier, @Nullable String documentId) {

        // Default case

        if (dossier == null) return null;

        // Finding doc

        if (!TextUtils.isEmpty(documentId)) for (Document document : dossier.getDocumentList())
            if (TextUtils.equals(document.getId(), documentId)) return document;

        // Else, finding any document

        return dossier.getDocumentList().isEmpty() ? null : dossier.getDocumentList().get(0);
    }


    public static @NonNull List<Document> getMainDocuments(@Nullable Dossier dossier) {

        // Default case

        if ((dossier == null) || (dossier.getDocumentList()) == null || (dossier.getDocumentList().isEmpty())) return new ArrayList<>();

        //

        ArrayList<Document> result = new ArrayList<>();
        for (Document document : dossier.getDocumentList())
            if (DocumentUtils.isMainDocument(dossier, document)) result.add(document);

        return result;
    }


    public static @NonNull List<Document> getAnnexes(@Nullable Dossier dossier) {

        // Default case

        if ((dossier == null) || (dossier.getDocumentList()) == null || (dossier.getDocumentList().isEmpty())) return new ArrayList<>();

        //

        ArrayList<Document> result = new ArrayList<>();
        for (Document document : dossier.getDocumentList())
            if (!DocumentUtils.isMainDocument(dossier, document)) result.add(document);

        return result;
    }


    public static @NonNull List<Dossier> getDeletableDossierList(@NonNull List<Bureau> parentBureauList, @NonNull List<Dossier> newDossierList) {

        List<Dossier> dossierToDeleteList = new ArrayList<>();

        for (Bureau parentBureau : parentBureauList) {
            if (parentBureau != null) {
                CollectionUtils.safeAddAll(dossierToDeleteList, parentBureau.getChildrenDossiers());
            }
        }

        dossierToDeleteList.removeAll(newDossierList);
        return dossierToDeleteList;
    }


    public static @NonNull List<Dossier> getAllChildrenFrom(@Nullable List<Bureau> bureauList) {

        List<Dossier> result = new ArrayList<>();

        if (bureauList != null) for (Bureau bureau : bureauList)
            CollectionUtils.safeAddAll(result, bureau.getChildrenDossiers());

        return result;
    }


}
