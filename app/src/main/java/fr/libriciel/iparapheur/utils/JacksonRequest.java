/*
 * <p>iParapheur Android<br/>
 * Copyright (C) 2016 Adullact-Projet.</p>
 *
 * <p>This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.</p>
 *
 * <p>This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package fr.libriciel.iparapheur.utils;

import android.util.Base64;
import android.util.Log;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import fr.libriciel.iparapheur.model.Account;

import static android.util.Base64.NO_WRAP;
import static java.nio.charset.StandardCharsets.UTF_8;


@Keep
public class JacksonRequest<T> extends JsonRequest<T> {

    private static final String LOG_TAG = "JacksonRequest";

    private final ObjectMapper objectMapper;
    private final TypeReference<T> typeRef;
    private final Response.Listener<T> listener;
    private final Account account;


    /**
     * Make a GET request and return a parsed object from JSON.
     *
     * @param url URL of the request to make
     */
    public JacksonRequest(int method, @NonNull String url, @Nullable Object body, @NonNull TypeReference<T> typeRef,
                          @NonNull Response.Listener<T> listener, @NonNull Response.ErrorListener errorListener,
                          @NonNull Account account, @NonNull ObjectMapper objectMapper) throws JsonProcessingException {

        super(method, url, objectMapper.writeValueAsString(body), listener, errorListener);
        this.typeRef = typeRef;
        this.listener = listener;
        this.objectMapper = objectMapper;
        this.account = account;
    }


    @Override public Map<String, String> getHeaders() throws AuthFailureError {

        String credentialData = String.format("%s:%s", account.getLogin(), account.getPassword());
        String loginHash = Base64.encodeToString(credentialData.getBytes(), NO_WRAP);

        Map<String, String> headers = new HashMap<>(super.getHeaders());
        headers.put("Authorization", String.format("Basic %s", loginHash));

        return headers;
    }


    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }


    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {

            Log.d(LOG_TAG, "Response string : " + new String(response.data, UTF_8));

            String json = new String(
                    response.data,
                    HttpHeaderParser.parseCharset(response.headers)
            );

            Log.d(LOG_TAG, "Response json   : " + new String(response.data, UTF_8));

            TypeReference<Void> voidRef = new TypeReference<Void>() {};

            return Response.success(
                    (typeRef.getType() == voidRef.getType()) ? null : objectMapper.readValue(json, typeRef),
                    HttpHeaderParser.parseCacheHeaders(response)
            );

        } catch (UnsupportedEncodingException | JsonProcessingException e) {
            return Response.error(new ParseError(e));
        }
    }


}
