/*
 * <p>iParapheur Android<br/>
 * Copyright (C) 2016 Adullact-Projet.</p>
 *
 * <p>This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.</p>
 *
 * <p>This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.</p>
 *
 * <p>You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.</p>
 */
package fr.libriciel.iparapheur.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.StatFs;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;

import com.google.common.io.Files;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

import fr.libriciel.iparapheur.R;
import fr.libriciel.iparapheur.model.Dossier;

import static android.content.Intent.ACTION_VIEW;
import static android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION;
import static android.widget.Toast.LENGTH_SHORT;
import static java.io.File.separator;


public class FilesUtils {

    private static final String LOG_TAG = "FileUtils";
    private static final String ASSET_CERTIFICATES_IMPORT_TUTORIAL = "IP-DOC-import_certificats_mobile_v1.3.pdf";
    private static final String DOSSIER_DATA_FOLDER_NAME = "dossiers";

    public static final @SuppressWarnings("squid:S2068") String SHARED_PREFERENCES_CERTIFICATES_PASSWORDS = ":iparapheur:shared_preferences_certificates_passwords";


    private static void copy(@NonNull File src, @NonNull File dst) {

        try (InputStream in = new FileInputStream(src);
             OutputStream out = new FileOutputStream(dst)) {

            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }

        } catch (IOException ex) {
            Log.e(LOG_TAG, "Error copying file", ex);
        }
    }


    public static @NonNull File getDirectoryForDossier(@NonNull Context context, @NonNull Dossier dossier) {

        File folder = new File(context.getExternalFilesDir(null) + separator + DOSSIER_DATA_FOLDER_NAME);
        if (!folder.exists())
            //noinspection ResultOfMethodCallIgnored
            folder.mkdirs();

        File directory = new File(folder.getAbsolutePath(), dossier.getId());

        if (!directory.mkdirs()) if (!directory.exists()) Log.e(LOG_TAG, "getDirectoryForDossier failed");

        return directory;
    }


    public static void launchCertificateTutorialPdfIntent(@NonNull Context context) {

        File pdfFile = createFileFromAsset(context, ASSET_CERTIFICATES_IMPORT_TUTORIAL);
        File fileFolder = context.getExternalFilesDir(null);

        // Default case
        // Should never happen, but hides IDE warnings

        if ((pdfFile == null) || (fileFolder == null))
            return;

        // The Asset file is in the internal folder, and cannot be shared directly.
        // We have to declare a FileProvider, and manage exported files or not...
        // Here, we simply move this asset into the external data folder.
        // That's easier, and takes way less code to manage.

        File exportablePdfFile = new File(fileFolder.getAbsolutePath() + separator + pdfFile.getName());
        if (!exportablePdfFile.exists())
            copy(pdfFile, exportablePdfFile);

        // View Intent, calling any PDF viewer

        Intent intentShareFile = new Intent(ACTION_VIEW);
        Uri pdfUri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", exportablePdfFile);
        intentShareFile.setDataAndType(pdfUri, "application/pdf");
        intentShareFile.addFlags(FLAG_GRANT_READ_URI_PERMISSION);
        context.startActivity(Intent.createChooser(intentShareFile, context.getString(R.string.Choose_an_app)));
    }


    private static @Nullable File getInternalCertificateStoragePath(@NonNull Context context) {

        boolean accessible = false;
        File rootFolder = context.getExternalFilesDir(null);
        File certificateFolder = null;

        if (rootFolder != null) {
            String certificatePath = rootFolder.getAbsolutePath() + separator + "certificates" + separator;
            certificateFolder = new File(certificatePath);
            accessible = certificateFolder.exists() || certificateFolder.mkdirs();
        }

        return accessible ? certificateFolder : null;
    }


    public static @NonNull List<File> getBksFromCertificateFolder(@NonNull Context context) {
        File folder = getInternalCertificateStoragePath(context);
        return (folder != null ? getBksFromFolder(folder) : new ArrayList<>());
    }


    private static @NonNull List<File> getBksFromFolder(@NonNull File folder) {
        List<File> jks = new ArrayList<>();

        if (folder.listFiles() != null) {
            for (File file : folder.listFiles()) {
                if (file.getName().endsWith("bks")) {
                    jks.add(file);
                }
            }
        }

        return jks;
    }


    public static boolean importCertificate(@NonNull Activity activity, @NonNull File certificateFile, @NonNull String password) {

        // Test password

        boolean bksOpeningSuccess = false;
        try {
            RsaSigner pkcs7signer = new RsaSigner(certificateFile.getAbsolutePath(), password, "", "");
            pkcs7signer.loadKeyStore();
            bksOpeningSuccess = true;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error", e);
            Toast.makeText(activity, R.string.import_error_message_opening_bks_file, LENGTH_SHORT).show();
        } catch (NoSuchAlgorithmException | UnrecoverableKeyException | KeyStoreException | CertificateException e) {
            Log.e(LOG_TAG, "Certificate error", e);
            Toast.makeText(activity, R.string.import_error_message_incompatible_device, LENGTH_SHORT).show();
        }

        // Stop on error

        if (!bksOpeningSuccess) return false;

        // Import to intern memory

        File to = new File(FilesUtils.getInternalCertificateStoragePath(activity), certificateFile.getName());

        try {
            //noinspection UnstableApiUsage
            Files.copy(certificateFile, to);

            SharedPreferences settings = activity.getSharedPreferences(SHARED_PREFERENCES_CERTIFICATES_PASSWORDS, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(certificateFile.getName(), password);
            editor.apply();

            Toast.makeText(activity, R.string.import_successful, LENGTH_SHORT).show();
            return true;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error moving file", e);
            Toast.makeText(activity, R.string.import_error_message_cant_copy_certificate, LENGTH_SHORT).show();
            return false;
        }
    }


    public static long getFreeSpace(@NonNull Context context) {

        File folder = new File(context.getExternalFilesDir(null) + separator + DOSSIER_DATA_FOLDER_NAME); // FIXME: NPE ??

        if (!folder.exists())
            //noinspection ResultOfMethodCallIgnored
            folder.mkdirs();

        StatFs statFs = new StatFs(folder.getAbsolutePath());

        return (statFs.getAvailableBlocksLong() * statFs.getBlockSizeLong());
    }


    /**
     * Creates a file form an Asset, through {@link #createFileFromInputStream}.
     */
    private static @Nullable File createFileFromAsset(@NonNull Context context, @NonNull String assetFileName) {
        AssetManager am = context.getAssets();

        try {
            InputStream inputStream = am.open(assetFileName);
            return createFileFromInputStream(context, inputStream, assetFileName);
        } catch (IOException ioException) {
            Log.e(LOG_TAG, "Error", ioException);
        }

        return null;
    }


    /**
     * Creates a file from a steam in the intern cacheDir/temp_files/ directory.
     */
    @SuppressWarnings("squid:S899") private static @Nullable File createFileFromInputStream(Context context, InputStream inputStream, String fileName) {

        File fileFolder = new File(context.getCacheDir().getAbsolutePath() + separator + "temp_files");

        // Default case

        boolean accessible = fileFolder.exists() || fileFolder.mkdirs();
        if (!accessible) {
            return null;
        }

        // Copying input into a new file

        File file = new File(fileFolder.getAbsolutePath() + separator + fileName);
        //noinspection ResultOfMethodCallIgnored
        file.delete(); // removing previous file, if exists.

        try (OutputStream outputStream = new FileOutputStream(file)) {

            byte[] buffer = new byte[1024];
            int length;

            while ((length = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error", e);
            file = null;
        }

        // Closing given input

        try {
            inputStream.close();
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error", e);
        }

        // Return result

        return file;
    }

}
