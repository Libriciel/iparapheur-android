/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.utils;

import androidx.annotation.StringRes;

import com.android.volley.ServerError;

import fr.libriciel.iparapheur.R;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class IParapheurException extends Exception {

    private final @StringRes int resId;
    private final String complement;


    public IParapheurException(String message) {
        super();
        this.resId = R.string.http_error_explicit;
        this.complement = message;
    }


    public IParapheurException(int httpCode) {
        super();
        this.resId = getMessageRes(httpCode);
        this.complement = null;
    }


    public IParapheurException(Exception e) {
        if (e.getCause() instanceof ServerError) {
            ServerError error = (ServerError) e.getCause();
            this.resId = getMessageRes(error.networkResponse.statusCode);
            this.complement = null;
        }
        else {
            this.resId = R.string.http_error_explicit;
            this.complement = e.getMessage();
        }
    }


    private static @StringRes int getMessageRes(int httpCode) {
        switch (httpCode) {
            case 400:
                return R.string.http_error_400;
            case 401:
                return R.string.http_error_401;
            case 403:
                return R.string.http_error_403;
            case 404:
                return R.string.http_error_404;
            case 405:
                return R.string.http_error_405;
            case 503:
                return R.string.http_error_503;
            default:
                return R.string.http_error_undefined;
        }
    }

}