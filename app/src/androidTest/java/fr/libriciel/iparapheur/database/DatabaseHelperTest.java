/*
 * iParapheur Android
 * Copyright (C) 2016-2020 Libriciel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.libriciel.iparapheur.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.RectF;
import android.preference.PreferenceManager;

import androidx.test.InstrumentationRegistry;
import androidx.test.filters.LargeTest;
import androidx.test.runner.AndroidJUnit4;

import com.j256.ormlite.dao.Dao;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import fr.libriciel.iparapheur.legacy.MyAccounts;
import fr.libriciel.iparapheur.model.Account;
import fr.libriciel.iparapheur.model.Action;
import fr.libriciel.iparapheur.model.Annotation;
import fr.libriciel.iparapheur.model.Bureau;
import fr.libriciel.iparapheur.model.Document;
import fr.libriciel.iparapheur.model.Dossier;
import fr.libriciel.iparapheur.model.PageAnnotations;
import fr.libriciel.iparapheur.utils.AccountUtils;
import fr.libriciel.iparapheur.utils.SerializableSparseArray;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;
import static fr.libriciel.iparapheur.model.Action.AVIS_COMPLEMENTAIRE;
import static fr.libriciel.iparapheur.model.Action.EMAIL;
import static fr.libriciel.iparapheur.model.Action.ENREGISTRER;
import static fr.libriciel.iparapheur.model.Action.JOURNAL;
import static fr.libriciel.iparapheur.model.Action.REJET;
import static fr.libriciel.iparapheur.model.Action.SECRETARIAT;
import static fr.libriciel.iparapheur.model.Action.SIGNATURE;
import static fr.libriciel.iparapheur.model.Action.TDT;
import static fr.libriciel.iparapheur.model.Action.TRANSFERT_SIGNATURE;
import static fr.libriciel.iparapheur.model.Action.VISA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;


@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@LargeTest
public class DatabaseHelperTest {

    private static DatabaseHelper sDbHelper;


    @BeforeClass public static void setup() {

        Context context = InstrumentationRegistry.getTargetContext();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences.edit().clear().commit();

        context.deleteDatabase(DatabaseHelper.DATABASE_NAME);
        sDbHelper = new DatabaseHelper(context);
    }


    @Test public void order01_createDefaultDemoAccount() throws Exception {
        Account demoAccount = sDbHelper.getAccountDao().queryBuilder().where().eq(Account.DB_FIELD_ID, AccountUtils.DEMO_ID).query().get(0);
        assertNotNull(demoAccount);
        sDbHelper.getAccountDao().delete(demoAccount);
        assertEquals(0, sDbHelper.getAccountDao().queryForAll().size());
    }


    @Test public void order02_onCreate() throws Exception {

        Dao<Account, Integer> accountDao = sDbHelper.getAccountDao();
        Dao<Bureau, Integer> bureauDao = sDbHelper.getBureauDao();
        Dao<Dossier, Integer> dossierDao = sDbHelper.getDossierDao();
        Dao<Document, Integer> documentDao = sDbHelper.getDocumentDao();

        // Checks

        assertEquals("Account", accountDao.getTableName());
        assertEquals("Desk", bureauDao.getTableName());
        assertEquals("Folder", dossierDao.getTableName());
        assertEquals("Document", documentDao.getTableName());
    }


    @Test public void order03_getAccountDao() throws Exception {

        Account account01 = new Account("id_01", "Title 01", "parapheur.test01.adullact.org", "login01", "password01", "tenant01", 1);
        Account account02 = new Account("id_02", "Title 02", "parapheur.test02.adullact.org", "login02", "password02", "tenant02", 2);
        Account account03 = new Account("id_03", "Title 03", "parapheur.test03.adullact.org", "login03", "password03", null, null);

        sDbHelper.getAccountDao().create(Arrays.asList(account01, account02, account03));

        Account account01db = sDbHelper.getAccountDao().queryForSameId(account01);
        Account account02db = sDbHelper.getAccountDao().queryForSameId(account02);
        Account account03db = sDbHelper.getAccountDao().queryForSameId(account03);

        // Checks

        assertEquals(3, sDbHelper.getAccountDao().queryForAll().size());

        assertEquals(account01.getApiVersion(), account01db.getApiVersion());
        assertEquals(account02.getApiVersion(), account02db.getApiVersion());
        assertNull(account03db.getApiVersion());
    }


    @Test public void order04_getBureauDao() throws Exception {

        Account account01 = sDbHelper.getAccountDao().queryBuilder().where().eq(Account.DB_FIELD_ID, "id_01").query().get(0);
        Account account02 = sDbHelper.getAccountDao().queryBuilder().where().eq(Account.DB_FIELD_ID, "id_02").query().get(0);
        Account account03 = sDbHelper.getAccountDao().queryBuilder().where().eq(Account.DB_FIELD_ID, "id_03").query().get(0);

        Bureau bureau01 = new Bureau("id_01", "Bureau 01", 21, 11);
        bureau01.setParent(account01);
        Bureau bureau02 = new Bureau("id_02", "Bureau 02 \"/%@&éè", 22, 12);
        bureau02.setParent(account01);
        Bureau bureau03 = new Bureau("id_03", null, 23, 13);
        bureau03.setParent(account02);

        sDbHelper.getBureauDao().create(Arrays.asList(bureau01, bureau02, bureau03));

        Bureau bureau01db = sDbHelper.getBureauDao().queryForSameId(bureau01);
        Bureau bureau02db = sDbHelper.getBureauDao().queryForSameId(bureau02);
        Bureau bureau03db = sDbHelper.getBureauDao().queryForSameId(bureau03);

        // Checks

        assertEquals(3, sDbHelper.getBureauDao().queryForAll().size());

        assertEquals(bureau01.getTitle(), bureau01db.getTitle());
        assertEquals(bureau02.getTitle(), bureau02db.getTitle());
        assertEquals("", bureau03db.getTitle());

        sDbHelper.getAccountDao().update(account01);
        sDbHelper.getAccountDao().update(account02);
        sDbHelper.getAccountDao().update(account03);

        assertEquals(2, account01.getChildrenBureaux().size());
        assertEquals(1, account02.getChildrenBureaux().size());
        assertEquals(0, account03.getChildrenBureaux().size());
    }


    @Test public void order05_getDossierDao() throws Exception {

        Bureau bureau01 = sDbHelper.getBureauDao().queryBuilder().where().eq(Bureau.DB_FIELD_ID, "id_01").query().get(0);
        Bureau bureau02 = sDbHelper.getBureauDao().queryBuilder().where().eq(Bureau.DB_FIELD_ID, "id_02").query().get(0);
        Bureau bureau03 = sDbHelper.getBureauDao().queryBuilder().where().eq(Bureau.DB_FIELD_ID, "id_03").query().get(0);

        assertNotNull(bureau01);
        assertNotNull(bureau02);
        assertNotNull(bureau03);

        HashSet<Action> actionsSet = new HashSet<>(Arrays.asList(ENREGISTRER, EMAIL, JOURNAL, SECRETARIAT, REJET, TDT, SIGNATURE, VISA, TRANSFERT_SIGNATURE, AVIS_COMPLEMENTAIRE));

        Dossier dossier01 = new Dossier("id_01", "Title 01", VISA, actionsSet, "t01", "st01", new Date(1392829477205L), new Date(1392829477205L), true);
        dossier01.setParent(bureau01);
        Dossier dossier02 = new Dossier("id_02", "Title 02 \"/%@&éè\"", TDT, actionsSet, "t01", "st02", new Date(1392829477205L), null, true);
        dossier02.setParent(bureau01);
        Dossier dossier03 = new Dossier("id_03", null, null, null, null, null, null, null, false);
        dossier03.setParent(bureau02);

        sDbHelper.getDossierDao().create(Arrays.asList(dossier01, dossier02, dossier03));

        Dossier dossier01db = sDbHelper.getDossierDao().queryBuilder().where().eq(Dossier.DB_FIELD_ID, "id_01").query().get(0);
        Dossier dossier02db = sDbHelper.getDossierDao().queryBuilder().where().eq(Dossier.DB_FIELD_ID, "id_02").query().get(0);
        Dossier dossier03db = sDbHelper.getDossierDao().queryBuilder().where().eq(Dossier.DB_FIELD_ID, "id_03").query().get(0);

        // Checks

        assertEquals(3, sDbHelper.getDossierDao().queryForAll().size());

        assertEquals(dossier01.getName(), dossier01db.getName());
        assertEquals(dossier02.getName(), dossier02db.getName());
        assertEquals("", dossier03db.getName());
        assertEquals(dossier01.getActionDemandee(), dossier01db.getActionDemandee());
        assertEquals(dossier02.getActionDemandee(), dossier02db.getActionDemandee());
        assertEquals(VISA, dossier03db.getActionDemandee());
        assertEquals(dossier01.getActions(), dossier01db.getActions());
        assertEquals(dossier02.getActions(), dossier02db.getActions());
        assertEquals(dossier03.getActions(), dossier03db.getActions());
        assertEquals(dossier01.getDateCreation(), dossier01db.getDateCreation());
        assertEquals(dossier02.getDateCreation(), dossier02db.getDateCreation());
        assertEquals(dossier03.getDateCreation(), dossier03db.getDateCreation());

        sDbHelper.getBureauDao().update(bureau01);
        sDbHelper.getBureauDao().update(bureau02);
        sDbHelper.getBureauDao().update(bureau03);

        assertEquals(2, bureau01.getChildrenDossiers().size());
        assertEquals(1, bureau02.getChildrenDossiers().size());
        assertEquals(0, bureau03.getChildrenDossiers().size());
    }


    @Test public void order06_getDocumentDao() throws Exception {

        Dossier dossier01 = sDbHelper.getDossierDao().queryBuilder().where().eq(Dossier.DB_FIELD_ID, "id_01").query().get(0);
        Dossier dossier02 = sDbHelper.getDossierDao().queryBuilder().where().eq(Dossier.DB_FIELD_ID, "id_02").query().get(0);
        Dossier dossier03 = sDbHelper.getDossierDao().queryBuilder().where().eq(Dossier.DB_FIELD_ID, "id_03").query().get(0);

        assertNotNull(dossier01);
        assertNotNull(dossier02);
        assertNotNull(dossier03);

        Document document01 = new Document("id_01", "name 01.pdf", 50000, true, true);
        document01.setPagesAnnotations(new SerializableSparseArray<PageAnnotations>());
        document01.setParent(dossier01);

        Annotation annotation = new Annotation("Author 01", 2, false, null, new RectF(0, 0, 10, 10), "Text 01", 0);
        PageAnnotations pageAnnotations = new PageAnnotations();
        pageAnnotations.add(annotation);
        SerializableSparseArray<PageAnnotations> serializableSparseArray = new SerializableSparseArray<>();
        serializableSparseArray.put(2, pageAnnotations);
        Document document02 = new Document("id_02", "name 02.pdf", 0, true, true);
        document02.setPagesAnnotations(serializableSparseArray);
        document02.setParent(dossier01);

        Document document03 = new Document("id_03", null, 50000, false, false);
        document03.setPagesAnnotations(null);
        document03.setParent(dossier02);

        sDbHelper.getDocumentDao().create(Arrays.asList(document01, document02, document03));

        Document document01db = sDbHelper.getDocumentDao().queryBuilder().where().eq(Document.DB_FIELD_ID, "id_01").query().get(0);
        Document document02db = sDbHelper.getDocumentDao().queryBuilder().where().eq(Document.DB_FIELD_ID, "id_02").query().get(0);
        Document document03db = sDbHelper.getDocumentDao().queryBuilder().where().eq(Document.DB_FIELD_ID, "id_03").query().get(0);

        // Checks

        assertEquals(3, sDbHelper.getDocumentDao().queryForAll().size());

        assertEquals(document01.getName(), document01db.getName());
        assertEquals(document02.getName(), document02db.getName());
        assertEquals("", document03db.getName());
        assertEquals(String.valueOf(document01.getPagesAnnotations()), String.valueOf(document01db.getPagesAnnotations()));
        assertEquals(String.valueOf(document02.getPagesAnnotations()), String.valueOf(document02db.getPagesAnnotations()));
        assertEquals(String.valueOf(document03.getPagesAnnotations()), String.valueOf(document03db.getPagesAnnotations()));

        sDbHelper.getDossierDao().update(dossier01);
        sDbHelper.getDossierDao().update(dossier02);
        sDbHelper.getDossierDao().update(dossier03);

        assertEquals(2, dossier01.getChildrenDocuments().size());
        assertEquals(1, dossier02.getChildrenDocuments().size());
        assertEquals(0, dossier03.getChildrenDocuments().size());
    }


    @Test public void order08_retrieveLegacyAccounts() throws Exception {

        Context context = InstrumentationRegistry.getTargetContext();
        MyAccounts myAccounts = MyAccounts.INSTANCE;

        // Cleanup

        List<Account> legacyAccountList = new ArrayList<>(myAccounts.getAccounts(context));

        for (Account legacyAccount : legacyAccountList)
            myAccounts.delete(context, legacyAccount);

        Account legacyAccount = myAccounts.addAccount(context);
        legacyAccount.setServerBaseUrl("baseurl.legacy");
        legacyAccount.setLogin("login_legacy");
        legacyAccount.setPassword("password_legacy");
        legacyAccount.setActivated(true);
        legacyAccount.setTitle("title_legacy");
        myAccounts.save(context, legacyAccount);

        // Check DB

        assertEquals(1, myAccounts.getAccounts(context).size());
        assertEquals(3, sDbHelper.getAccountDao().queryForAll().size());

        sDbHelper.retrieveLegacyAccounts(InstrumentationRegistry.getTargetContext());

        myAccounts.onSharedPreferenceChanged(getDefaultSharedPreferences(context), "account_");
        assertEquals(0, myAccounts.getAccounts(context).size());
        assertEquals(4, sDbHelper.getAccountDao().queryForAll().size());

        // Check Account

        Account dbAccount = sDbHelper.getAccountDao().queryBuilder().where().eq(Account.DB_FIELD_ID, legacyAccount.getId()).query().get(0);
        assertEquals(legacyAccount.getServerBaseUrl(), dbAccount.getServerBaseUrl());
        assertEquals(legacyAccount.getLogin(), dbAccount.getLogin());
        assertEquals(legacyAccount.getPassword(), dbAccount.getPassword());
        assertTrue(legacyAccount.isActivated());
        assertEquals(legacyAccount.getTitle(), dbAccount.getTitle());
        assertNull(legacyAccount.getApiVersion());
    }

}